import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import App from './App';
import Dev from '@/components/dev/Dev';
// import Index from '@/components/index/Index';
import Info from '@/components/info/Info';
import Lend from '@/components/lend/Lend';
import Product from '@/components/lend/child/Product';
import User from '@/components/user/User';
import * as serviceWorker from './serviceWorker';
import './main.scss';
import 'antd/dist/antd.css';
import Reg from '@/components/user/Reg';
import Mediadel from '@/components/info/Mediadel';
import Per from '@/components/user/Per';

ReactDOM.render(
  <Router>
    <Switch>
      <Route path = '/lend/child/product' component = { Product } />
      <Route path = '/dev' component = { Dev } />
      <Route path = '/info' component = { Info } />
      <Route path = '/lend' component = { Lend } />
      <Route path = '/user' component = { User } />
      <Route path = '/reg' component = { Reg } />
      <Route path="/Mediadel" component={ Mediadel } />
      <Route path = '/' component = { App } />
      <Route path = '/per' component = { Per } />
    </Switch>
  </Router>
  , document.getElementById('root'));
process.env.NODE_ENV === 'production' ? serviceWorker.register() : serviceWorker.unregister()
