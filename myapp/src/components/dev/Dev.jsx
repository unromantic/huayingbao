import React, { Component } from 'react';
import { Tabs } from 'antd';
import './dev.scss';
import Footer from '@/components/base/Footer';
import Bottom from '@/components/base/Bottom';
import GlobalHeader from '@/components/base/GlobalHeader';
import HeaderNav from '@/components/base/HeaderNav';
import GoTop from '@/components/base/GoTop';

const TabPane = Tabs.TabPane;
class Com extends Component {
  constructor (props) {
    super(props)
    this.state = {
      list: [
      ]
    }
  }
  callback(key) {
    console.log(key);
     }
  render () {
    return (
      <div className = "container">
        <GlobalHeader/>
        <HeaderNav/>
        <div className = "banner"></div>
        <Tabs onChange={this.callback} type="card">
          <TabPane tab="银行存管" key="1">
            <div className="abt_tt">什么是银行存管？</div>
            <div className="abt_main yewu">根据银监会等四部委发布的《网络借贷信息中介机构业务活动管理暂行办法》以及《网络借贷资金存管业务指引》，网贷平台必须选择符合条件的银行机构作为出借人与借款人的资金存管机构。银行存管上线后，资金交易环节均在银行系统中进行，平台与用户资金完全隔离，彻底杜绝资金池。2017年6月28日，华赢宝正式与银行签订存管协议，标志着华赢宝合规化建设步入新台阶。</div>
            <div className="abt_tt ling">银行存管流程图</div>
            <div className="abt_main ling_1"><img src={require('../../img/img_1.png')} alt="" /></div>
            <div className="abt_tt ling_6">为什么要接银行存管？</div>
            <div className="pc_page PC_MAIN2 ling_6">
                <div className="why_depository ling_1"><img src={require('../../img/img_2.png')} alt=""/></div>
                <div className="why_text_p4 ling_2">
                  <ul>
                    <li className="liebiao_1">
                      <p>对客户身份及交易授权进行认证</p>
                      <p>防止网贷机构非法挪用客户资金</p>
                    </li>
                    <li className="liebiao_2">
                      <p>完整记录网贷平台交易信息</p>
                      <p>并向出借人和借款人提供信息功能</p>
                    </li>
                  </ul>
                </div>  
                <div className="why_depository ling_3"><img src={require('../../img/img_3.png')} alt=""/></div>
                <div className="why_text_p4 ling_2">
                  <ul>
                    <li className="liebiao_3">
                      <p>设立单独的资金账户</p>
                      <p>实现各账户之间的有效隔离</p>
                    </li>
                    <li  className="liebiao_4">
                      <p>拥抱监管 合规经营新跨越</p>
                      <p>在监管政策的规定中，银行存管</p>
                    </li>
                  </ul>
                </div>
        </div>        
            <div className="abt_tt ling_6">资金存管的优势</div>
            <div className="abt_main ling_4">银行全程存管用户资金，提升资金安全，避免了平台跑路、资金池以及资金往来不透明的风险。</div>
            <div className="abt_main ling_5"><img src={require('../../img/img_4.png')} alt="" /></div>
            <div className="abt_tt ling_6">资金存管说明</div>
            <div className="abt_main ling_7">
              <p>主要权利义务描述：</p>
              <p>杭州华赢宝网络科技有限公司是资金存管业务中的委托人，银行是存管人。存管人接受委托人委托，开展客户资金存管业务。
委托人的工作职责主要包括：</p>
              <p>（一）负责网络借贷平台技术系统的持续开发及安全运营；</p>
              <p>（二）组织实施网络借贷信息中介机构信息披露工作，包括但不限于委托人基本信息、借贷项目信息、借款人基本信息及经营情况、各参与方信息等应向存管人充分披露的信息；</p>
              <p>（三）每日与存管人进行账务核对，确保系统数据的准确性；</p>
              <p>（四）妥善保管网络借贷资金存管业务活动的记录、账册、报表等相关资料，相关纸质或电子介质信息应当自借贷合同到期后保存5年以上。 存管人的工作职责主要包括：</p>
              <p>（一）为委托人开立网络借贷资金存管专用账户和自有资金账户，为出借人、借款人等在网络借贷资金存管专用账户下分别开立子账户，确保客户网络借贷资金和网络借贷信息中介机构自有资金分账管理，安全保管客户交易结算资金；</p>
              <p>（二）根据法律法规规定和存管合同约定，按照出借人与借款人发出的指令或业务授权指令，办理网络借贷资金的清算支付；</p>
              <p>（三）记录资金在各交易方、各类账户之间的资金流转情况；</p>
              <p>（四）妥善保管网络借贷资金存管业务相关的交易数据、账户信息、资金流水、存管报告等包括纸质或电子介质在内的相关数据信息和业务档案，相关资料应当自借贷合同到期后保存5年以上。</p>
              <p className="ziti">资金存管方式：直接存管</p>
              <p className="ziti">资金存管方法：双方业务系统对接</p>
              <p className="ziti">资金存管依据：
                <span className="ling_8">《网络借贷资金存管业务指引》</span>
              </p>
            </div>
          </TabPane>
          <TabPane tab="风险管理" key="2">
            <div className="abt_tt">华赢宝风险管理</div>
            <div className="abt_main yewu">
              <div className="icon_tt">
                <img src={require('../../img/tt_icon.png')} alt="" />
                <span className="ziti">借款审核</span>
                <span> — 严格的贷前审核流程，从源头控制贷款风险</span>
              </div>
            </div>
            <div className="abt_main yewu ling_10">
              <img src={require('../../img/img_5.png')} alt="" />
            </div>
            <div className="abt_main yewu">
              <div className="icon_tt">
                <img src={require('../../img/tt_icon.png')} alt=""/>
                <span className="ziti">小额分散</span>
                <span> — 小额贷款、出借分散，极大降低出借风险</span>
              </div>
            </div>
            <div className="abt_main yewu ling_10">
              <img src={require('../../img/img_6.png')} alt="" />
            </div>
            <div className="abt_main yewu">
              <div className="icon_tt"><img src={require('../../img/tt_icon.png')}alt="" />
                <span className="ziti">违约风险控制 </span>
                <span> — 贷后维护，逾期还款催收机制</span>
              </div>
            </div>        
            <div className="abt_main yewu ling_10">
              <img src={require('../../img/img_7.png')} alt="" />
            </div>
            <div className="abt_main yewu">
              <div className="icon_tt">
                <img src={require('../../img/tt_icon.png')} alt="" />
                <span className="ziti">风控把关 </span>
              </div>
            </div>
            <div className="abt_main yewu ling_10">
            华赢宝拥有高效的风险管理团队，并与同盾、白骑士等多家风控机构合作。公司基于自行研发的大数据风控平台对借款人进行全方位多角度的评估。对借款人的审核要点包括评估借款人职业的稳定性、居住的稳定性、项目的稳定性、家庭社交网络的稳定性等，通过对这些关键点的把握，确保公司筛选出最有守信的意愿和守信的能力的借款人。
            </div>
            <div className="abt_main yewu">
              <div className="icon_tt">
                <img src={require('../../img/tt_icon.png')} alt="" />
                <span className="ziti">法律保障 </span>
              </div>
            </div>
            <div className="abt_main yewu ling_10">
              根据《合同法》第196条规定“借款合同是借款人向贷款人借款，到期返还借款并支付利息的合同”，《合同法》允许普通民事主体之间发生借贷关系，并允许出借方到期可以收回本金和符合法律规定的利息。
            </div>
          </TabPane>
          <TabPane tab="股东背景" key="3">
            <div className="abt_tt">
              <p>华赢宝股东背景</p>
              <span className="ziti_1">股东背景（集团控股、上市背景）</span>
            </div>
          
            <div className="abt_main yewu">
              <div className="icon_tt">
                <img src={require('../../img/tt_icon.png')} alt="" />
                <span className="ziti">集团实力 </span>
              </div>
            </div>
          
            <div className="abt_main yewu">
              <p className="qingchu">华赢宝为支氏控股集团旗下互联网金融信息中介平台，办公地址为支氏控股集团自有办公大楼，股东背景强大。</p>
                  支氏控股集团由支华先生创办，注册地址在浙江省杭州市滨江区信诚路33号，注册并实缴资本3亿元。集团下属企业分布在房地产开发、建筑工程、市政建设、矿产资源、停车服务、进出口贸易及互联网金融、旅游、医疗、农业、创新培训等行业，总资产超过50亿元。支氏控股集团的前身是成立于2003年的杭州支华市政工程有限公司，十余年来，企业从小到大，由弱到强，逐步形成“实业+互联网+金融”齐头并进的发展趋势，作为建筑行业起家的支氏控股集团，始终不忘初心，不断做大做强实体产业。
            </div>

            <div className="abt_main yewu">
              <div className="icon_tt">
                <img src={require('../../img/tt_icon.png')} alt="" />
                <span className="ziti">集团业务 </span>
              </div>
            </div>

            <div className="abt_main yewu">
              <p className="qingchu"> 集团房地产板块主要与浙江省交通出借集团合作，目前合作项目位于杭州市淳安县千岛湖，该项目占地160亩，总计可建13万平方米住宅，截至2017年底已实现销售5亿元，未来3年的总销售额预计可突破10亿元。土地储备包括杭州市下沙街道37亩的开发用地和119亩的产业园用地，将在2018年进行开发，预计开发可售面积超过20万平米。
集团市政建设板块先后承接了杭州萧山国际机场T3航站楼、杭州地铁工程、杭州奥体博览城工程、杭州东站铁路枢纽工程、杭州市民中心工程、杭州钱江新城国际会议中心工程等重大项目，每年产值保持在20亿元左右。</p>
集团矿产资源板块通过拍卖方式取得了位于杭州市西湖区石矿的经营权，该石矿在有效经营期内的总销售额可达8亿元左右，目前日均销售额保持在100万元左右。
            </div>

            <div className="abt_main yewu">
              <img src={require('../../img/img_10.jpg')} alt="" />
            </div>

            <div className="abt_main yewu">
              <div className="icon_tt">
                <img src={require('../../img/tt_icon.png')} alt="" />
                <span className="ziti">固定资产  自有办公物业 </span>
              </div>
            </div>

            <div className="abt_main yewu">
              <span>华赢宝作为互联网企业，并不是轻资产运营，公司运营总部位于集团办公大楼内，是行业内极少数有自己独栋办公大楼的互联网金融公司，华赢宝安全稳健性远高于其他租办公室运营的轻资产互联网金融公司，</span>
              <span className="ling_11">
               <p className="qingchu">滨江办公楼：2栋</p> 
               <p className="qingchu">总层数：1栋10层，1栋5层</p>
               <p className="qingchu">总面积：15000m2</p>
               <p className="qingchu">总价值：评估价2亿元</p>
               <p className="qingchu">位置：杭州市滨江区信诚路33号支氏控股大厦</p>
               <p className="qingchu">优势：商业黄金地段、交通便利</p>
              </span>
          </div>
          
            <div className="abt_main yewu ling_10">
              <img src={require('../../img/img_8.png')} alt="" />
            </div>
          
            <div className="abt_main yewu ling_10">
              <img src={require('../../img/img_9.jpg')} alt="" />
            </div>
          
            <div className="abt_main yewu">
              <div className="icon_tt">
                <img src={require('../../img/tt_icon.png')} alt="" />
                <span className="ziti">股东上市公司背景 </span>
              </div>
            </div>
         
            <div className="abt_main yewu">
            杭州华赢宝网络科技有限公司具备香港主板上市企业背景,隶属于支氏控股集团有限公司。支氏控股集团在2 0 1 7 年斥资3 亿元完成对香港主板上市公司高锐中国物联
(HK.01682)的股权收购，控股并改名华隆金控，并已着手开展资本运作，增资扩股，整合装入集团旗下优质资产，提高集团综合实力和品牌影响力。

            </div>
         
            <div className="abt_main yewu ling_10">
              <img src={require('../../img/img_11.jpg')} alt="" />
            </div>

          </TabPane>
          <TabPane tab="网站安全" key="4">
            <div className="abt_tt">隐私安全</div>
            <div className="abt_main pad0">
              <div className="privacy_icon">
                  <ul>
                      <li className="touming ling_12">
                        <img src={require('../../img/skill.png')} alt="ss" />
                      </li>
                      <li className="touming ling_13">
                        <img src={require('../../img/jurisdiction.png')} alt="ss" />
                      </li>
                  </ul>
              </div>
              <div className="privacy_describe">
              <ul>
                  <li className="ling_12">
                      <dl className="move-left move-width-height2">
                          <dt>技术保障</dt>
                          <dd>华赢宝运用先进的安全技术保护用户在华赢宝账户中存储的个人信息、账户信息以及交易记录的安全。</dd>
                      </dl>
                  </li>
              </ul>
              <ul>
                  <li className="ling_13">
                      <dl className="move-right move-width-height3">
                          <dt>权限管理保障</dt>
                          <dd>华赢宝严格遵守国家相关的法律法规，对用户的隐私信息进行保护。未经同意，华赢宝不会向任何第三方公司、组织和个人披露用户个人信息、账户信息以及交易信息。</dd>
                      </dl>
                  </li>
              </ul>
              </div>
          </div>
            
            <div className="abt_tt">数据安全</div>
            <div className="abt_main pad0">
            
            <div className="safety ling_14">
                <ul>
                    <li className="safety_describe ling_12 ling_left">
                        <dl className="move-right move-width-height1 ysaq">
                            <dt>安全监测</dt>
                            <dd className="child_1">华赢宝拥有完善的安全监测系统，可以及时发现网站的非正常访问并做相应的安全响应。</dd>
                        </dl>
                    </li>
                    <li className="safety_icon ling_15 ling_right">
                        <img className="touming eyeimg" src={require('../../img/eye.png')} alt="ss" />
                    </li>
                </ul>
            </div>
            <div className="safety ling_16">
                <ul>
                    <li className="safety_icon ling_17">
                        <img className="touming bagimg" src={require('../../img/bag.png')} alt="ss" />
                    </li>
                    <li className="safety_describe ling_18">
                        <dl className="move-right move-width-height1">
                            <dt>数据加密</dt>
                            <dd className="child_1">华赢宝平台采用数据传输加密技术，对交易数据和记录不会有任何泄露。</dd>
                        </dl>
                    </li>
                </ul>
            </div>
        </div>
          
          </TabPane>
        </Tabs>
        <Footer />
        <Bottom />
        <GoTop />
      </div>
    )
  }
}

export default Com;
