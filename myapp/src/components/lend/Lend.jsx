import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import GlobalHeader from '@/components/base/GlobalHeader';
import HeaderNav from '@/components/base/HeaderNav';
import Bottom from '@/components/base/Bottom';
import Footer from '@/components/base/Footer';
import GoTop from '@/components/base/GoTop';
import './Lend.scss';
class Com extends Component {
  constructor (props) {
    super(props)
    this.state = {
      list: [
      ]
    }
  }

  render () {
    return (
      <div className = "lend">
        <GlobalHeader />
        <HeaderNav />
        <div className="biao_wrap">
          <div className="biao">
              <img src={require('./../../img/biao_img2x.png')} alt=""></img>
              <div className="biao_left">
                <div className="biao_head">
                  <span className="first">初次见面礼</span>
                  <span className="new">新手专享</span>
                  <span className="only">仅此一次</span>
                </div>
                <div className="biao_cont">
                  <ul>
                    <li>
                      <dl>
                        <dt><span id="interest5">15</span>%</dt>
                        <dd>借贷双方约定利率</dd>
                      </dl>
                    </li>
                    <li>
                      <dl>
                        <dt>
                          <span id="period5">10</span>天
                        </dt>
                        <dd>出借期限</dd>
                      </dl>
                    </li>
                    <li>
                      <dl>
                        <dt>
                          <span id="period5">197061.60</span>元
                        </dt>
                        <dd>剩余可投</dd>
                      </dl>
                    </li>
                  </ul>
                </div>
                <div className="biao_head">
                  <span id="name6">理财日产品第35期</span>
                  <span className="only">限时限量</span>
                  <span className="only">相约15号</span>
                </div>
                <div className="biao_cont">
                  <ul>
                    <li>
                      <dl>
                        <dt><span id="interest5">24</span>%</dt>
                        <dd>借贷双方约定利率</dd>
                      </dl>
                    </li>
                    <li>
                      <dl>
                        <dt>
                          <span id="period5">10</span>天
                        </dt>
                        <dd>出借期限</dd>
                      </dl>
                    </li>
                    <li>
                      <dl>
                        <dt>
                          <span id="period5">0.00</span>元
                        </dt>
                        <dd>剩余可投</dd>
                      </dl>
                    </li>
                  </ul>
                </div>
                <div className="biao_btn biao_btn1"><Link to="./lend/child/Product?id=1">立即出借</Link></div>
                <div className="biao_btn biao_btn2"><Link to="./lend/child/Product?id=2">立即出借</Link></div>
                <div className="biao_btn_bottom biao_btn_bottom1">
                  <div className='graph'></div>
                </div>
                <div className="biao_btn_bottom biao_btn_bottom2">
                  <div className='graph'></div>
                </div>
              </div>
            </div>
          <div className="biao">
            <img src={require('./../../img/biao_img22x.png')} alt=""></img>
            <div className="biao_left">
              <div className="biao_head">
                <span className="first">30天期产品第250期</span>
              </div>
              <div className="biao_cont">
                <ul>
                  <li>
                    <dl>
                      <dt><span id="interest5">8.5</span>%</dt>
                      <dd>借贷双方约定利率</dd>
                    </dl>
                  </li>
                  <li>
                    <dl>
                      <dt>
                        <span id="period5">30</span>天
                      </dt>
                      <dd>出借期限</dd>
                    </dl>
                  </li>
                  <li>
                    <dl>
                      <dt>
                        <span id="period5">2071862.96</span>元
                      </dt>
                      <dd>剩余可投</dd>
                    </dl>
                  </li>
                </ul>
              </div>
              <div className="biao_head">
                <span id="name6">90天期产品第44期</span>
              </div>
              <div className="biao_cont">
                <ul>
                  <li>
                    <dl>
                      <dt><span id="interest5">9.5</span>%</dt>
                      <dd>借贷双方约定利率</dd>
                    </dl>
                  </li>
                  <li>
                    <dl>
                      <dt>
                        <span id="period5">90</span>天
                      </dt>
                      <dd>出借期限</dd>
                    </dl>
                  </li>
                  <li>
                    <dl>
                      <dt>
                        <span id="period5">2658879.30</span>元
                      </dt>
                      <dd>剩余可投</dd>
                    </dl>
                  </li>
                </ul>
              </div>
              <div className="biao_btn biao_btn1"><Link to="./lend/child/Product">立即出借</Link></div>
              <div className="biao_btn biao_btn2"><Link to="./lend/child/Product">立即出借</Link></div>
              <div className="biao_btn_bottom biao_btn_bottom1">
                <div className='graph'></div>
              </div>
              <div className="biao_btn_bottom biao_btn_bottom2">
                <div className='graph'></div>
              </div>
            </div>
          </div>
          <div className="biao">
            <img src={require('./../../img/biao_img3.png')} alt=""></img>
            <div className="biao_left">
              <div className="biao_head">
                <span className="first">180天期产品第65期</span>
              </div>
              <div className="biao_cont">
                <ul>
                  <li>
                    <dl>
                      <dt><span id="interest5">11</span>%</dt>
                      <dd>借贷双方约定利率</dd>
                    </dl>
                  </li>
                  <li>
                    <dl>
                      <dt>
                        <span id="period5">180</span>天
                      </dt>
                      <dd>出借期限</dd>
                    </dl>
                  </li>
                  <li>
                    <dl>
                      <dt>
                        <span id="period5">405217.24</span>元
                      </dt>
                      <dd>剩余可投</dd>
                    </dl>
                  </li>
                </ul>
              </div>
              <div className="biao_head">
                <span id="name6">360天期产品第36期</span>
              </div>
              <div className="biao_cont">
                <ul>
                  <li>
                    <dl>
                      <dt><span id="interest5">12</span>%</dt>
                      <dd>借贷双方约定利率</dd>
                    </dl>
                  </li>
                  <li>
                    <dl>
                      <dt>
                        <span id="period5">360</span>天
                      </dt>
                      <dd>出借期限</dd>
                    </dl>
                  </li>
                  <li>
                    <dl>
                      <dt>
                        <span id="period5">807495.92</span>元
                      </dt>
                      <dd>剩余可投</dd>
                    </dl>
                  </li>
                </ul>
              </div>
              <div className="biao_btn biao_btn1"><Link to="./lend/child/Product">立即出借</Link></div>
              <div className="biao_btn biao_btn2"><Link to="./lend/child/Product">立即出借</Link></div>
              <div className="biao_btn_bottom biao_btn_bottom1">
                <div className='graph'></div>
              </div>
              <div className="biao_btn_bottom biao_btn_bottom2">
                <div className='graph'></div>
              </div>
            </div>
          </div>
        </div>
        <Footer />
        <Bottom />
        <GoTop />
      </div>
    )
  }
}

export default Com;
