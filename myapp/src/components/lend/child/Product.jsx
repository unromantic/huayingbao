import React, { Component } from 'react';
// import { Link } from 'react-router-dom';
import GlobalHeader from '@/components/base/GlobalHeader';
import HeaderNav from '@/components/base/HeaderNav';
import Bottom from '@/components/base/Bottom';
import Footer from '@/components/base/Footer';
import GoTop from '@/components/base/GoTop';
import { Tabs, Pagination  } from 'antd';
import './Product.scss';

const TabPane = Tabs.TabPane;

function callback(key) {
  console.log(key);
}

class Com extends Component {
  constructor (props) {
    super(props)
    this.state = {
      list: [
      ]
    }
  }
  componentDidMount () {
    fetch('https://aochuang.daxunxun.com/api/lenders')
      .then(res => res.json())
      .then(data => {
        // console.log(data)
        this.setState({
          list: data
        })
      })
  }
  render () {
    return (
      <div className = "lend">
        <GlobalHeader />
        <HeaderNav />
        <div className="content_wrap">
          <div className="purchase">
            <div className="main_left">
              <div className="left_tt">
                <span id="biao_tt">初次见面礼</span>
                <span id="tt_1" className="tt_1">新手专享</span>
                <span id="tt_2" className="tt_2">仅此一次</span>
                <span id="tt_3" className="tt_2 tt_3"></span>
              </div>
              <div className="left_mian">
                <ul>
                  <li>
                    <dl>
                      <dt>
                        <span id="interest">15.0</span>%
                      </dt>
                      <dd>借贷双方约定利率</dd>
                    </dl>
                  </li>
                  <li>
                    <dl>
                      <dt>
                        <span id="period">10</span>天
                      </dt><dd>出借期限</dd>
                    </dl>
                  </li>
                  <li>
                  <dl>
                    <dt>
                      <span id="surplus">192061.60</span>元
                    </dt>
                    <dd>剩余可投</dd>
                  </dl>
                  </li>
                </ul>
              </div>
              <div className="left_main2">
                <span>计息方式： </span>
                <span className="t1">到期还本付息</span>
                <span className="t2">万元收益：</span>
                <span id="yqshouyi" className="t1">41.10元</span>
              </div>
              <div className="left_main3">
                <ul>
                  <li className="canUseCoupon">
                    <img src={require('./../../../img/false.png')} alt='' />
                    <span>不可用优惠劵</span>
                  </li>
                  <li className="canRealized">
                  <img src={require('./../../../img/false.png')} alt='' />
                    <span>不可提前变现</span>
                  </li>
                  <li className="canConInvestment">
                  <img src={require('./../../../img/false.png')} alt='' />                    
                  <span>不可续投</span>
                  </li>
                </ul>
              </div>
            </div>
            <div className="main_right">
              <h5>账户余额：
                <i className="assetBalance"></i>0 元
                <a href="javascript:;">充值</a>
              </h5>
              <ul>
                <li>
                  <span>出借金额:</span>
                  <input className="hideRecord" type="number" />
                  <input className="H_money" type="number" placeholder="100元起投" />
                  <a className="allBuy" href="javascript:;">全投</a>
                </li>
                <li className="isCoupon">
                  <span>优惠券:</span>
                  <i className="couponChoice">不使用优惠券</i>
                  <a className="lookCoupon" href="javascript:;">查看</a>
                </li>
              </ul>
              <div className="dropDown">
                <ul>
                  <li className="onCoupon">不使用优惠券</li>
                </ul>
              </div>
              <p>预期收益金额:
                <i className="profit H_MoneyFrofit">0.00</i>元
              </p>
              <div className="isAgreement">
                <img className="check checkOn" id="check" src="images/login/checkOn.png"alt=''  />已同意
                <a href="./deposit.html" target="_blank">《定存计划服务协议》</a>
                <a href="./user.html" target="_blank">《用户授权委托书》</a>
                <a href="./loan.html" target="_blank">《风险揭示书》</a>
              </div>
              <div className="lends">
                <a href="javascript:;">立即出借</a>
              </div>
              <h4>温馨提示：市场有风险，出借需谨慎</h4>

              <div className="goLogin">
                <div className="goLoginBg"></div>
                <a id="goLoginBtn" href="javascript:;">立即登录</a>
              </div>
              <div className="repayment">
                <div className="repaymentBg"></div>
                <span>还款中…</span>
              </div>
              <div className="noProductBuy">
                <div className="noProductBuyBg"></div>
                <span>此产品不可购买…</span>
              </div>
            </div>         
          </div>
          {/* 登陆模态窗 */}
          <div className="loginBg"></div>
          <div className="investLogin registerForm">
            <h5>
              <span></span>登录
              <a id="close" href="/">关闭</a>
            </h5> 
            <h4>登录我的华赢宝账户</h4>
            <ul>
              <li>
                <span>手机号：</span>
                <input className="hideRecord" type="tel" />
                <input className="mobile" type="tel" maxLength="11" placeholder="请输入手机号" />
              </li>
              <li>
                <span>图形验证码：</span>
                <input className="codePic" type="text" maxLength="4" placeholder="请输入图形验证码" />
                <i className="v_container" id="v_container">
                  {/* <canvas id="verifyCanvas">您的浏览器版本不支持canvas</canvas> */}
                </i>
              </li>
              <li>
                <span>密码：</span>
                <input className="hideRecord" type="password" />
                <input className="password" type="password" maxLength="16" placeholder="请输入密码" />
              </li>
            </ul>
            <div className="isAgreement">
              <p>
                <img className="check" id="check" src="images/login/checkOn.png" alt='' />您的信息已使用128位SGC加密技术，保护数据传输安全
                </p>
            </div>
            <div className="registerBtn login">
              <button className="sameBtn loginBtn">立即登录</button>
            </div>
          </div>

          <div className="purchase purchase2">
            <div className="left_tt">
              <span>出借流程</span>
            </div>
            <img src={require('./../../../img/process.png')} alt='' />
            <div className="pur_main3">
              <ul>
                <li>立即出借</li>
                <li>
                  <span id="startDay">2019-01-03</span>
                  <span>开始计息</span>
                </li>
                <li>
                  <span id="endDay">2019-02-02</span>
                  <span>到期回款</span>
                </li>
                <li>提现至银行卡</li>
              </ul>
            </div>
          </div>

          <div className="purchase purchase3">
            <Tabs defaultActiveKey="1" onChange={callback}>
              <TabPane tab="产品详情" key="1">
                <div id="tab1" className="tab_content">
                  <div className="tab_tt">
                    <span className="tab_tt_icon"></span>借款描述
                  </div>
                  <div className="tab_txt">
                    <ul>
                      <li>1、借款人均为具有稳定收入来源的优质客户群体，借款用于短期消费或资金临时周转；</li>
                      <li>2、以小额分散的借款为主；</li>
                      {/* <li>3、通过信息认证、征信查询、人脸识别等大数据及风控系统，对借款人信息进行严格审核，且以工资卡自动代扣作为主要的还款保障，有效降低逾期风险，同时通过成熟的风控模型和大数据测评系统全面核实借款人信用记录、通讯、工作、家庭等相关信息。</li> */}
                    </ul>
                  </div>
                  <div className="tab_tt">
                    <span className="tab_tt_icon"></span>风控措施
                  </div>
                  <div className="tab_txt">
                    <ul>
                      {/* <li>1.合作资产机构对项目的本息支付提供连带责任担保，若借款人未能按期还款，由其进行代偿；</li> */}
                      <li>平台大数据风控严格审核</li>
                    </ul>
                  </div>
                  <div className="tab_tt">
                    <span className="tab_tt_icon"></span>产品原理
                  </div>
                  <img src={require('./../../../img/cpyl.png')} alt="" />
                </div>
              </TabPane>
              <TabPane tab="出借记录" key="2">
                <div id="tab2" className="tab_content">
                  <div className="tab2_tt">
                    <ul>
                      <li>出借用户</li>
                      <li>出借金额</li>
                      <li>出借时间</li>
                    </ul>
                  </div>
                  <div id="message">
                    <ul>
                    {
                      this.state.list.map((item, index) => {
                        return (
                          <li key={index}>
                            <img src={item.img} alt=""/>
                            <span>{item.name}</span>
                            <span>{item.money}</span>
                            <span>{item.lend_date}</span>
                          </li>
                        )
                      })
                    }
                    </ul>
                  </div>
                  <div className='Pagination'>
                    <Pagination defaultCurrent={1} total={500} />
                  </div>
                </div>
              </TabPane>
              <TabPane tab="借款人详情" key="3">
                <div id="tab3" className="tab_content">
                  <h5 className="tabTip">*为了保护借款人信息，仅展现部分数据</h5>
                  <div className="tab3_tt">
                    <ul>
                      <li>项目编号</li>
                      <li>借款人</li>
                      <li>身份证</li>
                      <li>借款用途</li>
                      <li>借款余额(元)</li>
                      <li>查看</li>
                    </ul>
                  </div>
                  <div id="datails">
                    <ul>
                      {
                        this.state.list.map((item, index) => {
                          return (
                            <li key={index}>
                              <span>{item.number}</span>
                              <span>{item.name}</span><span>{item.id}</span>
                              <span>{item.purpose}</span>
                              <span>{item.money}</span>
                              <span>
                                <a href="./purchaseDetail.html?id=324236">详情</a>
                              </span>
                            </li>
                          )
                        })
                      }
                    </ul>
                  </div>
                  <div className='Pagination'>
                    <Pagination defaultCurrent={1} total={500} />
                  </div>
                </div>
              </TabPane>
              <TabPane tab="常见问题" key="4">
                <div id="tab4" className="tab_content">
                  <div className="tab_tt">
                    <span className="tab_tt_icon tab_tt_icon2">1</span>什么是定存计划？
                  </div>
                  <div className="tab_txt">
                    <ul>
                      <li>答：“定存计划”是华赢宝推出的有锁定期的出借服务计划。出借人通过加入定存服务计划将其资金授权华赢宝出借给借款人，以出借于华赢宝网站公布的融资项目。华赢宝采用小额分散的智能投标、回款本金再出借的方式，提高资金的流动率和利用率，以期增加出借人收益。
                      加入定存计划的资金待匹配成功后，会进入锁定期，锁定期结束后，出借人在该计划内出借的标的将进行转让，全部标的转让完毕后，系统会将本金及应得收益部分资金转移到用户的华赢宝账户内。
                      </li>
                    </ul>
                  </div>
                  <div className="tab_tt">
                    <span className="tab_tt_icon tab_tt_icon2">2</span>如何确保转入定存计划的资金均用于真实借款？
                  </div>
                  <div className="tab_txt">
                    <ul>
                      <li>答：转入定存计划的资金自动匹配借款项目，可在借款合同中查看。</li>
                    </ul>
                  </div>
                  <div className="tab_tt">
                    <span className="tab_tt_icon tab_tt_icon2">3</span>产品是否可以提前变现？
                  </div>
                  <div className="tab_txt">
                    <ul>
                      <li>答：可以，购买产品未到期前申请提前变现，需收取变现金额0.5%手续费，并取消购买时所使用的满减券类优惠和相应积分奖励，如产品标明不可变现，则无法申请提前变现。</li>
                    </ul>
                  </div>
                  <div className="tab_tt">
                    <span className="tab_tt_icon tab_tt_icon2">4</span>产品出借时有哪些续投方式？
                  </div>
                  <div className="tab_txt">
                    <ul>
                      <li>答：用户出借时需选择资金到期处理方式，且在产品到期之前，用户可随时变更续投方式。</li>
                      <li>（1）本金续投：选择此续投方式，产品到期后本金产生的收益将在到期日回到账户余额，本金将自动续投本产品（单利），续出借金的收益率为续投累计周期对应的产品收益率（例如30天期产品第二次续投按照60天期产品计算收益，第三次按照90天期产品计算收益，以此类推）。</li>
                      <li>（2）本息续投：选择此续投方式，产品到期后本金和收益将自动续投本产品（单利），续出借金的收益率为续投累计周期对应的产品收益率（例如30天期产品第二次续投按照60天期产品计算收益，第三次按照90天期产品计算收益，以此类推）。</li>
                      <li>（3）到期后转活期：选择此续投方式，产品到期后本金和收益将自动存入零钱包，按照零钱包的利率继续产生收益。</li>
                      <li>（4）不续投：到期后本金以及产生的收益当日回到账户余额。</li>
                    </ul>
                  </div>
                </div>
              </TabPane>
            </Tabs>
          </div>
        </div>
        <Footer />
        <Bottom />
        <GoTop />
      </div>
    )
  }
}

export default Com;
