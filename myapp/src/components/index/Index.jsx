import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import GlobalHeader from '@/components/base/GlobalHeader';
import HeaderNav from '@/components/base/HeaderNav';
import Banner from '@/components/base/Banner';
import Biao1 from '@/components/base/Biao1';
import Biao2 from '@/components/base/Biao2';
import Biao3 from '@/components/base/Biao3';
import Partner from '@/components/base/Partner';
import FooterBanner from '@/components/base/FooterBanner';
import Footer from '@/components/base/Footer';
import Bottom from '@/components/base/Bottom';
import GoTop from '@/components/base/GoTop';

class App extends Component {
  render() {
    return (
      <div className="App">
        <GlobalHeader />
        <HeaderNav />
        <Banner />
        <div className="trade">
          <span>累积交易额<span><p>33.84</p>亿元</span></span>
          <span>累计注册数<span><p>233175</p>人</span></span>
          <span>为用户累计赚取收益<span><p>2578.80</p>万元</span></span>
          <span>安全运营时间<span><p>1171</p>天</span></span>
          <span><Link to="/info?id=1">更多数据>></Link></span>
        </div>
        <div className="tongji_box ">
          <div className="tongji_content">
            <ul>
              <li>
                <img src={require('./../../img/icon_1.png')} alt=""></img>
                <h1>实时透明</h1>
                <p>全面信息披露制度</p>
              </li>
              <li>
                <img src={require('./../../img/icon_2.png')} alt=""></img>
                <h1>普惠金融</h1>
                <p>专注小额分散的消费金融</p></li>
              <li>
                <img src={require('./../../img/icon_3.png')} alt=""></img>
                <h1>专业风控</h1>
                <p>完善的风控体系和贷后管理</p></li>
              <li>
                <img src={require('./../../img/icon_4.png')} alt=""></img>
                <h1>实力雄厚</h1>
                <p>支氏集团控股</p></li>
            </ul>
          </div>
        </div>
        <Biao1 />
        <Biao2 />
        <Biao3 />
        <Partner />
        <FooterBanner />
        <Footer />
        <Bottom />
        <GoTop />
      </div>
    );
  }
}

export default App;
