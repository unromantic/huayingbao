import React, { Component } from 'react';
import './info.scss';
class Com extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return(
      <div id="tab4" className="tab_content">
        <div className="survey">
            <div className="survey_tt">
                交易数据 <span>截止日期：2019年1月2日</span>
            </div>
            <div className="survey_main">
                <ul>
                    <li>
                        <span><img src={require('./../../img/op1.png')} alt=""/></span>
                        <dl>
                            <dt>
                                <p id="investMoneyTotal">3394803044.844</p>
                            </dt>
                            <dd>累计交易总金额（元）</dd>
                        </dl>
                        <div className="contain">
                            <div className="content">
                                自华赢宝成立起，华赢宝撮合完成的所有交易总和
                            </div>
                            <s>
                                <i className="contentStr"></i>
                            </s>
                        </div>
                    </li>
                    <li>
                        <span><img src={require('./../../img/op2.png')} alt=""/></span>
                        <dl>
                            <dt>
                                <p className="investCountTotal">28780</p>
                            </dt>
                            <dd>累计交易笔数（笔）</dd>
                        </dl>
                        <div className="contain">
                            <div className="content">
                                自华赢宝成立起，华赢宝撮合完成的借贷交易笔数总和
                            </div>
                            <s>
                                <i className="contentStr"></i>
                            </s>
                        </div>
                    </li>
                    <li>
                        <span><img src={require('./../../img/op3.png')} alt=""/></span>
                        <dl>
                            <dt>
                                <p id="borrowingMoney">120858710.74</p>
                            </dt>
                            <dd>借贷余额（元）</dd>
                        </dl>
                        <div className="contain">
                            <div className="content">
                                借贷余额：在投总额
                            </div>
                            <s>
                                <i className="contentStr"></i>
                            </s>
                        </div>
                    </li>
                    <li>
                        <span><img src={require('./../../img/op4.png')} alt=""/></span>
                        <dl>
                            <dt>
                                <p id="borrowingCount">10498</p>
                            </dt>
                            <dd>借贷余额笔数（笔）</dd>
                        </dl>
                        <div className="contain">
                            <div className="content">
                                借贷余额笔数：在投总额笔数
                            </div>
                            <s>
                                <i className="contentStr"></i>
                            </s>
                        </div>
                    </li>
                    <li>
                        <span><img src={require('./../../img/op5.png')} alt=""/></span>
                        <dl>
                            <dt>
                                <p id="totalInterest">0</p>
                            </dt>
                            <dd>利息余额（元）</dd>
                        </dl>
                        <div className="contain">
                            <div className="content">
                                利息余额：在投总额的利息
                            </div>
                            <s>
                                <i className="contentStr"></i>
                            </s>
                        </div>
                    </li>
                    <li>
                        <span><img src={require('./../../img/op6.png')} alt=""/></span>
                        <dl>
                            <dt>
                                <p>0</p>
                            </dt>
                            <dd>关联关系借款余额(元)</dd>
                        </dl>
                        <div className="contain">
                            <div className="content">
                                我们平台暂无：默认为0笔
                            </div>
                            <s>
                                <i className="contentStr"></i>
                            </s>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div className="survey">
            <div className="survey_tt">
                借款人数据 <span>截止日期：2019年1月2日</span>
            </div>
            <div className="survey_main">
                <ul>
                    <li>
                        <span><img src={require('./../../img/op7.png')} alt=""/></span>
                        <dl>
                            <dt>
                                <p id="borrowUserCountTotal">71746</p>
                            </dt>
                            <dd>累计借款用户</dd>
                        </dl>
                        <div className="contain">
                            <div className="content">
                                在平台借款成功，累计借款人总数（去重）
                            </div>
                            <s>
                                <i className="contentStr"></i>
                            </s>
                        </div>
                    </li>
                    <li>
                        <span><img src={require('./../../img/op7.png')} alt=""/></span>
                        <dl>
                            <dt>
                                <p id="borrowUserCount">9822</p>
                            </dt>
                            <dd>当前借款用户</dd>
                        </dl>
                        <div className="contain">
                            <div className="content">
                                平台（在借）借款人总数
                            </div>
                            <s>
                                <i className="contentStr"></i>
                            </s>
                        </div>
                    </li>
                    <li>
                        <span><img src={require('./../../img/op8.png')} alt=""/></span>
                        <dl>
                            <dt>
                                <p id="borrowUserMoney">298.75</p>
                            </dt>
                            <dd>人均累计借款金额（元）</dd>
                        </dl>
                        <div className="contain">
                            <div className="content">
                                自平台运营上线起，借款人累计交易总额与累计借款人总数之比
                            </div>
                            <s>
                                <i className="contentStr"></i>
                            </s>
                        </div>
                    </li>
                    <li>
                        <span><img src={require('./../../img/op9.png')} alt="" /></span>
                        <dl>
                            <dt>
                                <p id="borrowUserBigMoney">0.46</p>
                            </dt>
                            <dd>最大单一借款人待还金额占比(%)</dd>
                        </dl>
                        <div className="contain">
                            <div className="content">
                                最大一单借款人借款余额与累计借款总额之比
                            </div>
                            <s>
                                <i className="contentStr"></i>
                            </s>
                        </div>
                    </li>
                    <li>
                        <span><img src={require('./../../img/op9.png')} alt="" /></span>
                        <dl>
                            <dt>
                                <p id="borrowUserTenMoney">2.47</p>
                            </dt>
                            <dd>前十大借款人待还金额占比(%)</dd>
                        </dl>
                        <div className="contain">
                            <div className="content">
                                借款最多的前十户借款人的借款余额与累计借款总额之比
                            </div>
                            <s>
                                <i className="contentStr"></i>
                            </s>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div className="survey">
            <div className="survey_tt">
                出借人信息 <span>截止日期：2019年1月2日</span>
            </div>
            <div className="survey_main">
                <ul>
                    <li>
                        <span><img src={require('./../../img/op7.png')} alt="" /></span>
                        <dl>
                            <dt>
                                <p className="investCountTotal">28780</p>
                            </dt>
                            <dd>累计出借人数量（人）</dd>
                        </dl>
                        <div className="contain">
                            <div className="content">
                                在平台出借成功，累计出借人总数（去重）
                            </div>
                            <s>
                                <i className="contentStr"></i>
                            </s>
                        </div>
                    </li>
                    <li>
                        <span><img src={require('./../../img/op7.png')} alt="" /></span>
                        <dl>
                            <dt>
                                <p id="investCount">2448</p>
                            </dt>
                            <dd>当前出借人数量（人）</dd>
                        </dl>
                        <div className="contain">
                            <div className="content">
                                平台（在投）出借人总数
                            </div>
                            <s>
                                <i className="contentStr"></i>
                            </s>
                        </div>
                    </li>
                    <li>
                        <span><img src={require('./../../img/op8.png')} alt="" /></span>
                        <dl>
                            <dt>
                                <p id="investCountMoney">117957.02</p>
                            </dt>
                            <dd>人均累计出借金额（元）</dd>
                        </dl>
                        <div className="contain">
                            <div className="content">
                                自平台运营上线起，平台出借人累计交易总额与累计出借人总数之比
                            </div>
                            <s>
                                <i className="contentStr"></i>
                            </s>
                        </div>
                    </li>
                    <li>
                        <span><img src={require('./../../img/op9.png')} alt="" /></span>
                        <dl>
                            <dt>
                                <p id="investCountBigMoney">0.01</p>
                            </dt>
                            <dd>最大单户出借余额占比(%)</dd>
                        </dl>
                        <div className="contain">
                            <div className="content">
                                出借金额最大的出借人出借金额总和与累计出借总额之比
                            </div>
                            <s>
                                <i className="contentStr"></i>
                            </s>
                        </div>
                    </li>
                    
                    <li>
                        <span><img src={require('./../../img/op9.png')} alt="" /></span>
                        <dl>
                            <dt>
                                <p id="investCountTenMoney">0.08</p>
                            </dt>
                            <dd>最大十户出借余额占比(%)</dd>
                        </dl>
                        <div className="contain">
                            <div className="content">
                                出借最多的前十户出借人的出借金额总和与累计借款总额之比
                            </div>
                            <s>
                                <i className="contentStr"></i>
                            </s>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div className="survey">
            <div className="survey_tt">
                逾期数据 <span>截止日期：2019年1月2日</span>
            </div>
            <div className="survey_main">
                <ul>
                    <li>
                        <span><img src={require('./../../img/op10.png')} alt="" /></span>
                        <dl>
                            <dt>
                                <p>0</p>
                            </dt>
                            <dd>逾期金额（元）</dd>
                        </dl>
                    </li>
                    <li>
                        <span><img src={require('./../../img/op11.png')} alt="" /></span>
                        <dl>
                            <dt>
                                <p>0</p>
                            </dt>
                            <dd>金额逾期率(%)</dd>
                        </dl>
                    </li>
                    <li>
                        <span><img src={require('./../../img/op12.png')} alt="" /></span>
                        <dl>
                            <dt>
                                <p>0</p>
                            </dt>
                            <dd>逾期笔数（笔)</dd>
                        </dl>
                    </li>
                    <li>
                        <span><img src={require('./../../img/op13.png')} alt="" /></span>
                        <dl>
                            <dt>
                                <p>0</p>
                            </dt>
                            <dd>项目逾期率(%)</dd>
                        </dl>
                    </li>
                    <li>
                        <span><img src={require('./../../img/op14.png')} alt="" /></span>
                        <dl>
                            <dt>
                                <p>0</p>
                            </dt>
                            <dd>代偿金额（元）</dd>
                        </dl>
                    </li>
                    <li>
                        <span><img src={require('./../../img/op12.png')} alt="" /></span>
                        <dl>
                            <dt>
                                <p>0</p>
                            </dt>
                            <dd>代偿笔数（笔）</dd>
                        </dl>
                    </li>
                    <li>
                        <span><img src={require('./../../img/op10.png')} alt="" /></span>
                        <dl>
                            <dt>
                                <p>0</p>
                            </dt>
                            <dd>逾期90天（不含）以上金额</dd>
                        </dl>
                    </li>
                    <li>
                        <span><img src={require('./../../img/op10.png')} alt="" /></span>
                        <dl>
                            <dt>
                                <p>0</p>
                            </dt>
                            <dd>逾期90天（不含）以上笔数</dd>
                        </dl>
                    </li>
                    <li>
                        <span><img src={require('./../../img/op13.png')} alt="" /></span>
                        <dl>
                            <dt>
                                <p>0</p>
                            </dt>
                            <dd>项目分级逾期率1~90（天）</dd>
                        </dl>
                    </li>
                    <li>
                        <span><img src={require('./../../img/op13.png')} alt="" /></span>
                        <dl>
                            <dt>
                                <p>0</p>
                            </dt>
                            <dd>项目分级逾期率91~180（天）</dd>
                        </dl>
                    </li>
                    <li>
                        <span><img src={require('./../../img/op13.png')} alt="" /></span>
                        <dl>
                            <dt>
                                <p>0</p>
                            </dt>
                            <dd>项目分级逾期率180天以上</dd>
                        </dl>
                    </li>
                    <li>
                        <span><img src={require('./../../img/op13.png')} alt="" /></span>
                        <dl>
                            <dt>
                                <p>0</p>
                            </dt>
                            <dd>金额分级逾期率1~91（天）</dd>
                        </dl>
                    </li>
                    <li>
                        <span><img src={require('./../../img/op11.png')} alt="" /></span>
                        <dl>
                            <dt>
                                <p>0</p>
                            </dt>
                            <dd>金额分级逾期率91~180（天）</dd>
                        </dl>
                    </li>
                    <li>
                        <span><img src={require('./../../img/op11.png')} alt="" /></span>
                        <dl>
                            <dt>
                                <p>0</p>
                            </dt>
                            <dd>金额分级逾期率180天以上</dd>
                        </dl>
                    </li>
                </ul>
            </div>
        </div>
        <div className="survey">
            <div className="survey_tt">
                投资人收费标准
            </div>
            <table className="table">
                <tbody>
                    <tr>
                        <th><span>业务类型</span></th>
                        <th>用户注册</th>
                        <th>开通存管账户</th>
                        <th>充值</th>
                        <th>出借管理</th>
                        <th>提现</th>
                    </tr>
                    <tr>
                        <td><span>收费标准</span></td>
                        <td><b>免费</b></td>
                        <td><b>免费</b></td>
                        <td>充值费用，暂全部由华赢宝承担，不向出借人收取</td>
                        <td>目前免费</td>
                        <td><i>30</i>天内免费提现<i>5</i>次，超过次数收取<i>2</i>元/笔的费用</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div className="survey">
            <div className="survey_tt">
                借款人收费标准
            </div>
            <table className="table">
                <tbody>
                    <tr>
                        <th><span>业务类型</span></th>
                        <th>用户注册</th>
                        <th>开通存管账户</th>
                        <th>充值</th>
                        <th>提现</th>
                        <th>借款服务</th>
                    </tr>
                    <tr>
                        <td><span>收费标准</span></td>
                        <td><b>免费</b></td>
                        <td><b>免费</b></td>
                        <td><b>免费</b></td>
                        <td><b>免费</b></td>
                        <td>向借款人收取服务费</td>
                    </tr>
                </tbody>
            </table>
        </div>        
    </div>
    )
  }
}

export default Com;