import React, { Component} from 'react';
import './info.scss';
class Com extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return(
      <div className="tab_content">
        <div className="abt_tt">业务介绍</div>
        <div className="abt_main yewu">
            <div className="icon_tt"><img src={require('./../../img/tt_icon.png')} alt="" /><span>资产安全</span></div>
        </div>
        <div className="abt_main yewu">互联网金融的本质还是金融，随着监管政策的落实及网贷平台银行存管的上线，优质稀缺资产是决定网贷平台出借收益和安全性的根本因素。华赢宝主打优质安全的消费金融产品。
        </div>
        <div className="abt_main yewu">
            <div className="icon_tt"><img src={require('./../../img/tt_icon.png')} alt="" /><span>产品特点</span></div> 
        </div>
        <div className="abt_main"><span className="font_c">优质借款人：</span>借款人均为具有稳定收入来源的优质青年客户群体，月收入在2000-5000元，借款用于短期消费借款，工作稳定，违约成本高。</div>
        <div className="abt_main"><span className="font_c">金额小、期限短：</span>借款期限一般不超过3个月，单笔借款金额90%以上不超过5000元，单笔借款期限短、金额小，借款人极度分散；</div>
        <div className="abt_main yewu"><span className="font_c">多重风险控制：</span>通过信息认证、征信查询、人脸识别、本地黑名单数据等等大数据及风控系统，对借款人信息进行严格审核，有效降低逾期风险，同时通过成熟的风控模型和大数据测评系统全面核实借款人信用记录、通讯、工作、家庭等相关信息。</div>
        <div className="abt_main yewu">
            <div className="icon_tt"><img src={require('./../../img/tt_icon.png')} alt=""/><span>产品原理</span></div>
        </div>
        <div className="abt_main yewu">
            <img src={require('./../../img/cpjs.jpg')} alt="" style={{ marginLeft:"100px" }}/>
        </div>
        <div className="abt_main yewu">
            <div className="icon_tt"><img src={require('./../../img/tt_icon.png')} alt=""/><span>合作机构</span></div>
        </div>
        <div className="abt_main yewu">
            <img src={require('./../../img/jigou.png')} alt="" style={{ marginLeft:"100px" }}/>
        </div>
        <div className="abt_main yewu">
            <div className="icon_tt"><img src={require('./../../img/tt_icon.png')} alt=""/><span>资金安全</span></div>
        </div>
        <div className="abt_main"><span className="font_c">小额贷款出借分散：</span>单个借款人单笔借款最多不超过5000元，智能匹配，出借人可分散出借给多个借款债权人，极大降低出借风险</div>
        <div className="abt_main"><span className="font_c">项目真实透明：</span>三类信息，公开透明，有凭证，可追溯，透明无虚假（出借人可随时到公司交流考察）</div>        
    </div>
    )
  }
}

export default Com;