import React, {
  Component
} from 'react';
class Com extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return(
      <div className="tab_content">
        <div className="tab7_tt"><div id="diamond"></div><span>相关法律法规披露</span></div>
        <div className="law">
            <ul>
                <li><a href="https://www.huayingbaolicai.com/fagui1.html" target="_blank" rel="noopener noreferrer">《网络借贷风险认知书》</a></li>
                <li><a href="https://www.huayingbaolicai.com/fagui2.html" target="_blank" rel="noopener noreferrer">《网络借贷禁止性行为提示书》</a></li>
                <li><a href="https://www.huayingbaolicai.com/fagui3.html" target="_blank" rel="noopener noreferrer">《资金来源合法承诺书》</a></li>
                <li><a href="https://www.huayingbaolicai.com/fagui4.html" target="_blank" rel="noopener noreferrer">出借人风险评估制度</a></li>
                <li><a href="https://www.huayingbaolicai.com/fagui5.html" target="_blank" rel="noopener noreferrer">网贷风险教育</a></li>
                <li><a href="https://www.huayingbaolicai.com/fagui6.html" target="_blank" rel="noopener noreferrer">相关法律法规</a></li>
            </ul>
        </div>
    </div>
    )
  }
}

export default Com;