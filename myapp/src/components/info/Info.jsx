import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import GlobalHeader from '@/components/base/GlobalHeader';
import HeaderNav from '@/components/base/HeaderNav';
import Footer from '@/components/base/Footer';
import Bottom from '@/components/base/Bottom';
import GoTop from '@/components/base/GoTop';
import Default from './Default';
import Team from './Team';
import Business from './Business';
import Operate from './Operate';
import Media from './Media';
import Company from './Company';
import Mechanism from './Mechanism';
import Law from './Law';
import InfoNav from './Infonav';
class Com extends Component {
  constructor (props) {
    super(props)
    this.state = {
      list: [
        {
          path: '/Info/Default',
          name: '关于华赢宝'
        },
        {
          path: '/Info/Team',
          name: '团队介绍'
        },
        {
          path: '/Info/Business',
          name: '业务介绍'
        },
        {
          path: '/Info/Operate',
          name: '运营数据'
        },
        {
          path: '/Info/Media',
          name: '媒体报道'
        },
        {
          path: '/Info/Company',
          name: '公司动态'
        },
        {
          path: '/Info/Mechanism',
          name: '从业机构信息'
        },
        {
          path: '/Info/Law',
          name: '相关法律法规'
        }       
      ]
    }
  }

  render () {
    return (
      <div className = "container">
        <GlobalHeader />
        <HeaderNav />
        <div className="meiti"></div>
        <InfoNav 
        list = { this.state.list }
        />
        <Switch>
          <Route path="/Info/default" component={ Default } />
          <Route path="/Info/team" component={ Team } />
          <Route path="/Info/business" component={ Business } />
          <Route path="/Info/operate" component={ Operate } />
          <Route path="/Info/media" component={ Media } />
          <Route path="/Info/company" component={ Company } />
          <Route path="/Info/mechanism" component={ Mechanism } />
          <Route path="/Info/law" component={ Law } />
          <Redirect from = '/Info' to="/Info/default" />
        </Switch>
        <Footer />
        <Bottom />
        <GoTop />
      </div>
    )
  }
}

export default Com;
