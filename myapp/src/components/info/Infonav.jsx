import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './info.scss';
class Com extends Component {
  constructor (props) {
    super(props)
    this.state = {}
  }
  changecolor (index) {
    let cc = document.querySelectorAll('.cc');
    cc[1].style.color = '#222c57';
    cc[2].style.color = '#222c57';
    cc[3].style.color = '#222c57';
    cc[4].style.color = '#222c57';
    cc[5].style.color = '#222c57';
    cc[6].style.color = '#222c57';
    cc[7].style.color = '#222c57';
    cc[0].style.color = '#222c57';
    cc[index].style.color = '#ef7a46';
    cc[index].style.textDecoration = 'none';
    
  }
  render () {
    return (
      <nav className = "headerbox1">
        <ul>
          {
            this.props.list.map((item, index) => {
              return (
                <li key = { index }>
                  <Link to={ item.path } onClick = { this.changecolor.bind(this, index) } className="cc" replace> { item.name }</Link>
                </li>
              )
            })
          }
        </ul>
      </nav>
    )
  }
}

export default Com;
