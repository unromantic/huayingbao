import React, { Component} from 'react';
import { Link, NavLink } from 'react-router-dom';
import './info.scss';
class Com extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return(
      <div id="tab5" className="tab_content">
        <div className="abt_tt">媒体报道</div>
        <div className="abt_main yewu med">
            <img src={require('./../../img/news5.png')} alt="" />
            <dl>
                <dt><NavLink to="/Mediadel" target="_blank" replace>「华赢宝征文活动」有华赢宝的日子，树桩吐绿，鸟语花香</NavLink></dt>
                <dd>2017年11月08日</dd>
                <dd>《道德经》第四十二章曰：道生一，一生二，二生三，三生万物......拟以此句，喻华赢宝惠泽近二十万出借用户，其意深远。
                </dd>
            </dl>
        </div>
        <div className="abt_main yewu med">
            <img src={require('./../../img/news6.png')} alt="" />
            <dl>
                <dt><Link to="/Mediadel" replace>华赢宝累计成交突破10亿元</Link></dt>
                <dd>2017年11月10日</dd>
                <dd>11月初，华赢宝累计出借金额突破10亿元。对于一个两年的互金平台而言，华赢宝踏踏实实走好了每一步，与用户一起写下圆满的答卷。截至2017年11月10日，华赢宝出借用户数18.19万人，累计交易成功出借金额10.32亿元，客户赚取收益累计已达670万元，安全运营时间755天。
                </dd>
            </dl>
        </div>
        <div className="abt_main yewu med">
            <img src={require('./../../img/news6.png')} alt=""/>
            <dl>
                <dt><Link to="/Mediadel" replace>华赢宝获批ICP证，积极响应监管政策变化</Link></dt>
                <dd>2017年10月27日</dd>
                <dd>ICP成稀缺资质 全国仅10%平台获批据数据显示全国正常运营的互金平台数量近2000家。其中约有两百余家家平台拥有有效的ICP经营性许可证，约占网贷行业正常运营平台总数量的10%，ICP经营许可证成为挡在多数平台合规之路前的拦路虎。
                </dd>
            </dl>
        </div>        
        <div className="abt_main yewu med">
            <img src={require('./../../img/news2.png')} alt=""/>
            <dl>
                <dt><Link to="/Mediadel" replace>支氏控股集团华赢宝发布验资报告</Link></dt>
                <dd>2017年07月07日</dd>
                <dd>2016年10月28日，中国互联网金融协会正式发布《互联网金融信息披露 个体网络借贷》标准(T/NIFA  1—2016)(下称信披标准)，信披标准明确提出，互联网金融行业从业机构应主动披露从业机构的注册资本与实缴资本。</dd>
            </dl>
        </div>
        <div className="abt_main yewu med">
            <img src={require('./../../img/news3.png')} alt=""/>
            <dl>
                <dt><Link to="/Mediadel" replace>华赢宝携手三家优质资产对接，加速合规建设</Link></dt>
                <dd>2017年08月07日</dd>
                <dd>华赢宝上线以来一直把风控和获取优质资产放在首位，为了满足出借人对优质资产的需求，华赢宝选择与实力较强的机构合作扩展业务线;在国家消费拉动经济增长的政策环境下，瞄准C端用户刚需、开发消费金融和现金贷市场等，合理布局资产端。华赢宝将和合作机构打造全新的“消费分期”和“现金贷”产品，该产品以其小额、分散、灵活、收益更高的特点，受到了出借人的热捧。</dd>
            </dl>
        </div>
        <div className="abt_main yewu med">
            <img src={require('./../../img/news4.png')} alt=""/>
            <dl>
                <dt><Link to="/Mediadel" replace>华赢宝赞助2017萧山广场舞大赛完美结束</Link></dt>
                <dd>2017年08月30日</dd>
                <dd>8月27号由区委宣传部、区妇联、萧山日报、萧山网主办，华赢宝移动平台赞助的2017萧山广场舞大赛完美结束。来自瓜沥、北干、靖江、临浦、宁围、新塘等地的13支队伍闪耀登场，决战王者之夜，经过4个多小时的激烈角逐艺飞艺术团问鼎冠军宝座。</dd>
            </dl>
        </div>
    </div>
    )
  }
}

export default Com;