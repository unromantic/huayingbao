import React, {
  Component
} from 'react';
class Com extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  change (index) {
    let olis = document.querySelectorAll('.oli');
    olis[0].className = 'oli';
    olis[1].className = 'oli';
    olis[2].className = 'oli';
    olis[3].className = 'oli';
    olis[4].className = 'oli';
    olis[5].className = 'oli';
    olis[6].className = 'oli';
    olis[index].className = 'oli active';
    let ouls = document.querySelectorAll('.oul');
    console.log(ouls)
    console.log(index);
    if (index%2 === 1) {
      ouls[0].style.display = 'none';
      ouls[1].style.display = 'block';
    } else {
      ouls[1].style.display = 'none';
      ouls[0].style.display = 'block';
    }    
  }
  next () {
    let ouls = document.querySelectorAll('.oul');
    if (ouls[0].style.display === 'none') {
      ouls[0].style.display = 'block';
      ouls[1].style.display = 'none'           
    } else {
      ouls[0].style.display = 'none';
      ouls[1].style.display = 'block'
    }
  }
  previous () {
    let ouls = document.querySelectorAll('.oul');
    if (ouls[0].style.display === 'none') {
      ouls[0].style.display = 'block';
      ouls[1].style.display = 'none'           
    } else {
      ouls[0].style.display = 'none';
      ouls[1].style.display = 'block'
    }
  }
  render() {
    return(
      <div className="tab_content1">
        <div className="message">
            <ul className="oul">
              <li>
                <div className="bold2"></div>
                <span className="float_left">
                <span>尊敬的华赢宝用户：</span>
                <p>元旦假日期间华赢宝平台业务运行如下：</p>
                <p>支付取现：</p>
                <p>2018年12月30日至2019年1月1日可正常投资支付，所有取现操作均顺延至节后首个工作日2019年1月2日处理到账。</p>
                <p>2018年12月29日为工作日，可正常取现，当天16:30前取现皆可当天到账。</p>
                <p>客服维护：</p>
                <p>元旦节假日期间，会有客服人员值班为大家服务，时间为每日9点至21:00。</p>                
                <p>放假期间微信后台咨询暂无法回复，大家遇到问题可联系客服电话4001190717咨询解决。</p>
                <p>感谢大家对华赢宝的支持与理解，祝大家假期快乐！</p>
                </span>
                <span className="float_right">2018-12-28</span>
              </li>
              <li>
                <div className="bold2"></div>
                <span className="float_left">
                <span>尊敬的华赢宝用户：</span>
                <p>由于系统默认设置，于12月2日推送了积分年底清零的相关消息通知。现情况说明如下：</p>
                <p>1.积分已在7月3日取消，并在当天根据每位用户实际积分数量兑换成华赢币。</p>
                <p>2.用户可登陆后查看华赢币数量。</p>
                <p>3.华赢币可在华赢币商城和兑换专区使用。</p>
                <p>4.华赢币不可转让，不会过期清零。若有变动，会及时通知。</p>
                <p>5.用户可通过每日登陆、投资获得更多华赢币。由于原来的积分至年底会清零，所以有推送设置。但积分已取消，而对应的系统默认推送设置因技术原因未取消，所以导致用户收到积分清零的提示，大家可以忽略此条信息。给您带来的不便敬请谅解！华赢币目前可正常使用，不会过期清零，请大家放心。若有任何疑问，可咨询客服电话4001190717.</p>                
                </span>
                <span className="float_right">2018-12-03</span>
              </li>
              <li>
                <div className="bold2"></div>
                <span className="float_left">
                <span>尊敬的华赢宝用户：</span>
                <p>为给各位用户提供更稳定流畅的理财体验，目前平台已更换支付通道为汇聚支付。汇聚支付采用协议支付模式，支持银行更多，额度更高，购买环节更便捷，体验更顺畅。更换后，用户购买取现将不受影响，仅需再确定预留手机号码即可。</p>
                <p>使用APP的用户请将华赢宝APP更新至最新版本，即可体验新支付通道带来的流畅体验。</p>
                <p>对于额度较低的银行，也可至网页端使用网银充值，若有任何银行都可以咨询客服人员4001190717.</p>               
                </span>
                <span className="float_right">2018-11-24</span>
              </li>
              <li>
                <div className="bold2"></div>
                <span className="float_left">
                <span>尊敬的华赢宝用户：</span>
                <p>华赢币兑换版本已上线，微信端和app端都可以进行兑换，目前iOS版本审核需要1到2天时间，期间或许出现页面错乱问题，可到微信端正常使用，安卓版本不受影响。感谢您的支持与理解，给您带来的不便敬请谅解！</p>               
                </span>
                <span className="float_right">2018-07-12</span>
              </li>
              <li>
                <div className="bold2"></div>
                <span className="float_left">
                <span>尊敬的华赢宝用户：</span>
                <p>为方便用户更换银行卡，现平台已上线银行卡解绑功能。请点击您的头像中-我的银行卡-修改银行卡，提交相关资料申请解绑更换。客服人员会及时查看并进行审核，审核成功及失败都会有推送反馈（APP推送、微信推送、站内信）通知，请您时刻关注。</p>
                <p>若有任何问题，请联系客服人员400-1190-717进行咨询。
</p>                
                </span>
                <span className="float_right">2018-06-11</span>
              </li>
              <li>
                <div className="bold2"></div>
                <span className="float_left">
                <span>尊敬的华赢宝用户：</span>
                <p>PC端网银支付充值功能已上线，APP上一些维护的银行卡，在PC端可以充值支付啦~
</p>
                <p>主要功能有以下几点：</p>
                <p>1.PC端支持网银充值，购买标的。</p>
                <p>2.网银充值有支持的银行卡列表，但具体额度根据各个银行要求。目前APP不支持的农行、招行等都可以在网银端充值啦！</p>
                <p>3.目前第一版本，只支持充值购买，提现操作将在下个版本上线。如果用户需要提现的话可以到APP里操作哦！</p>
                <p>4.另外，用户身份认证银行绑卡也需要到APP中操作哦。</p>                
                </span>
                <span className="float_right">2018-06-01</span>
              </li>
              <li>
                <div className="bold2"></div>
                <span className="float_left">
                <span>尊敬的华赢宝用户：</span>
                <p>接连连支付通知，以下几个银行暂不支持支付，为不影响您的理财体验，请您联系客服及时更换银行卡，具体支持银行卡可查询银行卡额度列表。</p>
                <p>农业银行、邮储银行、招商银行、民生银行、华夏银行、江苏银行暂不支持使用。若恢复将在第一时间通知。</p>
                <p>更换银行卡请在微信公众号中联系客服人员，提交相关资料审核。给您带来的不便敬请谅解！</p>                
                </span>
                <span className="float_right">2018-05-15</span>
              </li>
              <li>
                <div className="bold2"></div>
                <span className="float_left">
                <span>尊敬的华赢宝用户：</span>
                <p>接连连支付通道通知，五一劳动节期间华赢宝平台业务运行如下：</p>
                <p>支付取现：</p>
                <p>4月29日至5月1日可正常投资支付，所有取现操作均顺延至节后首个工作日5月2日处理到账。</p>
                <p>客服维护：</p>
                <p>五一假期，会有客服人员值班为大家服务，时间为每日9点至21:00。</p>
                <p>放假期间微信后台咨询暂无法回复，大家遇到问题可联系客服电话4001190717咨询解决。</p>
                <p>感谢大家对华赢宝的支持与理解，祝大家假期快乐！</p>
                </span>
                <span className="float_right">2018-04-27</span>
              </li>
              <li>
                <div className="bold2"></div>
                <span className="float_left">
                <span>尊敬的华赢宝用户：</span>
                <p>接连连支付通道通知，清明节期间华赢宝平台业务运行如下：</p>
                <p>支付取现：</p>
                <p>4月5日至4月7日可正常投资支付，所有取现操作均顺延至节后首个工作日4月8日处理到账。</p>
                <p>客服维护：</p>
                <p>清明节假期，会有客服人员值班为大家服务，时间为每日9点至21:00。</p>
                <p>放假期间微信后台咨询暂无法回复，大家遇到问题可联系客服电话4001190717咨询解决。</p>
                <p>感谢大家对华赢宝的支持与理解，祝大家假期快乐！</p>
                </span>
                <span className="float_right">2018-04-03</span>
              </li>
              <li>
                <div className="bold2"></div>
                <span className="float_left">
                <span>尊敬的华赢宝用户：</span>
                <p>尊敬的华赢宝用户： 接连连支付通知，江苏银行、光大银行认证支付业务暂不支持，使用江苏银行级光大银行卡的用户请及时联系客服人员更换银行卡。给您带来的不便敬请谅解！</p>                
                </span>
                <span className="float_right">2018-03-30</span>
              </li>
            </ul>
            <ul className="oul">
              <li>
                <div className="bold2"></div>
                <span className="float_left">
                <span>尊敬的华赢宝用户：</span>
                <p>PC端网银支付充值功能已上线，APP上一些维护的银行卡，在PC端可以充值支付啦~
</p>
                <p>主要功能有以下几点：</p>
                <p>1.PC端支持网银充值，购买标的。</p>
                <p>2.网银充值有支持的银行卡列表，但具体额度根据各个银行要求。目前APP不支持的农行、招行等都可以在网银端充值啦！</p>
                <p>3.目前第一版本，只支持充值购买，提现操作将在下个版本上线。如果用户需要提现的话可以到APP里操作哦！</p>
                <p>4.另外，用户身份认证银行绑卡也需要到APP中操作哦。</p>                
                </span>
                <span className="float_right">2018-06-01</span>
              </li>
              <li>
                <div className="bold2"></div>
                <span className="float_left">
                <span>尊敬的华赢宝用户：</span>
                <p>接连连支付通知，以下几个银行暂不支持支付，为不影响您的理财体验，请您联系客服及时更换银行卡，具体支持银行卡可查询银行卡额度列表。</p>
                <p>农业银行、邮储银行、招商银行、民生银行、华夏银行、江苏银行暂不支持使用。若恢复将在第一时间通知。</p>
                <p>更换银行卡请在微信公众号中联系客服人员，提交相关资料审核。给您带来的不便敬请谅解！</p>                
                </span>
                <span className="float_right">2018-05-15</span>
              </li>
              <li>
                <div className="bold2"></div>
                <span className="float_left">
                <span>尊敬的华赢宝用户：</span>
                <p>接连连支付通道通知，五一劳动节期间华赢宝平台业务运行如下：</p>
                <p>支付取现：</p>
                <p>4月29日至5月1日可正常投资支付，所有取现操作均顺延至节后首个工作日5月2日处理到账。</p>
                <p>客服维护：</p>
                <p>五一假期，会有客服人员值班为大家服务，时间为每日9点至21:00。</p>
                <p>放假期间微信后台咨询暂无法回复，大家遇到问题可联系客服电话4001190717咨询解决。</p>
                <p>感谢大家对华赢宝的支持与理解，祝大家假期快乐！</p>
                </span>
                <span className="float_right">2018-04-27</span>
              </li>
              <li>
                <div className="bold2"></div>
                <span className="float_left">
                <span>尊敬的华赢宝用户：</span>
                <p>接连连支付通道通知，清明节期间华赢宝平台业务运行如下：</p>
                <p>支付取现：</p>
                <p>4月5日至4月7日可正常投资支付，所有取现操作均顺延至节后首个工作日4月8日处理到账。</p>
                <p>客服维护：</p>
                <p>清明节假期，会有客服人员值班为大家服务，时间为每日9点至21:00。</p>
                <p>放假期间微信后台咨询暂无法回复，大家遇到问题可联系客服电话4001190717咨询解决。</p>
                <p>感谢大家对华赢宝的支持与理解，祝大家假期快乐！</p>
                </span>
                <span className="float_right">2018-04-03</span>
              </li>
              <li>
                <div className="bold2"></div>
                <span className="float_left">
                <span>尊敬的华赢宝用户：</span>
                <p>尊敬的华赢宝用户： 接连连支付通知，江苏银行、光大银行认证支付业务暂不支持，使用江苏银行级光大银行卡的用户请及时联系客服人员更换银行卡。给您带来的不便敬请谅解！</p>                
                </span>
                <span className="float_right">2018-03-30</span>
              </li>
              <li>
                <div className="bold2"></div>
                <span className="float_left">
                <span>尊敬的华赢宝用户：</span>
                <p>元旦假日期间华赢宝平台业务运行如下：</p>
                <p>支付取现：</p>
                <p>2018年12月30日至2019年1月1日可正常投资支付，所有取现操作均顺延至节后首个工作日2019年1月2日处理到账。</p>
                <p>2018年12月29日为工作日，可正常取现，当天16:30前取现皆可当天到账。</p>
                <p>客服维护：</p>
                <p>元旦节假日期间，会有客服人员值班为大家服务，时间为每日9点至21:00。</p>                
                <p>放假期间微信后台咨询暂无法回复，大家遇到问题可联系客服电话4001190717咨询解决。</p>
                <p>感谢大家对华赢宝的支持与理解，祝大家假期快乐！</p>
                </span>
                <span className="float_right">2018-12-28</span>
              </li>
              <li>
                <div className="bold2"></div>
                <span className="float_left">
                <span>尊敬的华赢宝用户：</span>
                <p>由于系统默认设置，于12月2日推送了积分年底清零的相关消息通知。现情况说明如下：</p>
                <p>1.积分已在7月3日取消，并在当天根据每位用户实际积分数量兑换成华赢币。</p>
                <p>2.用户可登陆后查看华赢币数量。</p>
                <p>3.华赢币可在华赢币商城和兑换专区使用。</p>
                <p>4.华赢币不可转让，不会过期清零。若有变动，会及时通知。</p>
                <p>5.用户可通过每日登陆、投资获得更多华赢币。由于原来的积分至年底会清零，所以有推送设置。但积分已取消，而对应的系统默认推送设置因技术原因未取消，所以导致用户收到积分清零的提示，大家可以忽略此条信息。给您带来的不便敬请谅解！华赢币目前可正常使用，不会过期清零，请大家放心。若有任何疑问，可咨询客服电话4001190717.</p>                
                </span>
                <span className="float_right">2018-12-03</span>
              </li>
              <li>
                <div className="bold2"></div>
                <span className="float_left">
                <span>尊敬的华赢宝用户：</span>
                <p>为给各位用户提供更稳定流畅的理财体验，目前平台已更换支付通道为汇聚支付。汇聚支付采用协议支付模式，支持银行更多，额度更高，购买环节更便捷，体验更顺畅。更换后，用户购买取现将不受影响，仅需再确定预留手机号码即可。</p>
                <p>使用APP的用户请将华赢宝APP更新至最新版本，即可体验新支付通道带来的流畅体验。</p>
                <p>对于额度较低的银行，也可至网页端使用网银充值，若有任何银行都可以咨询客服人员4001190717.</p>               
                </span>
                <span className="float_right">2018-11-24</span>
              </li>
              <li>
                <div className="bold2"></div>
                <span className="float_left">
                <span>尊敬的华赢宝用户：</span>
                <p>华赢币兑换版本已上线，微信端和app端都可以进行兑换，目前iOS版本审核需要1到2天时间，期间或许出现页面错乱问题，可到微信端正常使用，安卓版本不受影响。感谢您的支持与理解，给您带来的不便敬请谅解！</p>               
                </span>
                <span className="float_right">2018-07-12</span>
              </li>
              <li>
                <div className="bold2"></div>
                <span className="float_left">
                <span>尊敬的华赢宝用户：</span>
                <p>为方便用户更换银行卡，现平台已上线银行卡解绑功能。请点击您的头像中-我的银行卡-修改银行卡，提交相关资料申请解绑更换。客服人员会及时查看并进行审核，审核成功及失败都会有推送反馈（APP推送、微信推送、站内信）通知，请您时刻关注。</p>
                <p>若有任何问题，请联系客服人员400-1190-717进行咨询。
</p>                
                </span>
                <span className="float_right">2018-06-11</span>
              </li>
            </ul>
        </div>
        <div className="jump">
          <ul className="Pagination">         
            <li onClick = { this.previous.bind(this) }>上一页</li>
            <li onClick = { this.change.bind(this ,0) } className="oli">1</li>
            <li onClick = { this.change.bind(this ,1) } className="oli">2</li>
            <li onClick = { this.change.bind(this ,2) } className="oli">3</li>
            <li onClick = { this.change.bind(this ,3) } className="oli">4</li>
            <li onClick = { this.change.bind(this ,4) } className="oli">5</li>
            <li onClick = { this.change.bind(this ,5) } className="oli">6</li>
            <li onClick = { this.change.bind(this ,6) } className="oli">7</li>
            <li onClick = { this.next.bind(this) }>下一页</li>
          </ul>
        </div>
    </div>
    )
  }
}

export default Com;