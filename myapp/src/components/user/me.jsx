import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import {withRouter} from 'react-router'
import './user.scss';
import axios from 'axios'
class Com extends Component {
  constructor(props) {
    super(props)
    this.state = {
      list: []
    }
  }
  render() {
    return(
    	<div className="memberRight">
				<div className="overview">
					<h4>
						<span></span>账户总览
					</h4>
					<div className="overviewDl">
						<dl>
							<dt>总资产：</dt>
							<dd><i className="assetProfit">0.00</i>元</dd>
						</dl>
						<dl>
							<dt>昨日收益：</dt>
							<dd><i className="lastProfit">0.00</i>元</dd>
						</dl>
						<dl>
							<dt>累计收益：</dt>
							<dd><i className="totalProfit">0.00</i>元</dd>
						</dl>
						<dl>
							<dt>账户余额：<span className="assetBalance">0.00</span>元</dt>
							<dd>
								<a className="memberRecharge" href="./u-recharge.html">充值</a>
								<a className="memberCash" href="./u-cash.html">提现</a>
							</dd>
						</dl>
					</div>
					<p>总资产=定期资产(本金+当前收益）+活期资产(本金+当前收益）+账户余额</p>
				</div>

				<div className="assetList">
					<h4>
						<span></span>资产记录
					</h4>
					<div className="assetSelect">
						<ul>
							<li className="selected" data-type="">全部记录</li>
							<li data-type="1">持有资产</li>
							<li data-type="2">已到期资产</li>
						</ul>
					</div>

					<div className="assetMain">
						<div className="assetMainTitle">
							<ul>
								<li>
									<span>项目名称</span>
									<span>年化率(%)</span>
									<span>投资金额(元)</span>
									<span>收益(元)</span>
									<span>投资时间</span>
									<span>还款时间</span>
									<span>状态</span>
									<span>操作</span>
								</li>
							</ul>
						</div>
						<div className="assetMainSize">
							<ul></ul>
						</div>
						<div className="assetMainTip">暂无记录</div>
					</div>
				</div>
			</div>          
    )
  }
}

export default withRouter(Com);