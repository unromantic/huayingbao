import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { Link } from 'react-router-dom';
import GlobalHeader from '@/components/base/GlobalHeader';
import HeaderNav from '@/components/base/HeaderNav';
import Footer from '@/components/base/Footer';
import Bottom from '@/components/base/Bottom';
import Me from './me';
import account from './account';
import './user.scss';
class Com extends Component {
  constructor (props) {
    super(props)
    this.state = {
      list: [
      ]
    }
  }
	selected (i) {
		let oas = document.querySelectorAll('.oa');
		console.log(oas);
		oas[0].className = 'oa';
		oas[1].className = 'oa';
		oas[i].className = 'oa selected';
	}
	componentDidMount () {
		let userMobile = document.querySelector('#userMobile');		
		userMobile.innerHTML = localStorage.getItem('username');
		console.log(userMobile.innerHTML)
	}
  render () {
    return (
      <div className = "container">
      <GlobalHeader />
      <HeaderNav />
      <div  className="zzz">
	      <div className="member">
					<h5>
						您所在的位置: <a href="./index.html">首页</a> &gt; <a href="./u-member.html">个人中心</a> &gt; 我的资产
					</h5>
					<div className="memberMain">
						<div className="memberLeft">
							<div className="title">
								<div className="titleLeft">
									<img src={require('./../../img/per.png')}/>
								</div>
								<div className="titleRight">
									<p>您好，欢迎来到华赢宝！</p>
									<p className="userMobile">用户名：<i id="userMobile"></i></p>
								</div>
							</div>
							<ul>
								<li>
									<Link className="oa selected" to="/per/me" onClick = { this.selected.bind(this ,0) } replace>我的资产</Link>
								</li>
								<li>
									<Link to="/per/account" className="oa" onClick = { this.selected.bind(this ,1) } replace>账户充值</Link>
								</li>
							</ul>
						</div>
						<div>
							<Switch>        
			          <Route path="/per/me" component={ Me } />
			          <Route path="/per/account" component={ account } />
			          <Redirect from = '/per' to="/per/me" />
			        </Switch>
						</div>
					</div>
				</div>
      </div>
      
      <Footer />
      <Bottom />
      </div>
    )
  }
}

export default Com;
