import React, { Component } from 'react';
import {withRouter} from 'react-router'
import './user.scss';
class Com extends Component {
  constructor(props) {
    super(props)
    this.state = {
      list: []
    }
  }
  render() {
    return(
    	<div className="memberRight">
				<div className="recharge">
					<h4>
						<span></span>网银充值
					</h4>
					<div className="rechargeMainTitle">
						<div className="rechargeMoney">
							<h5>充值金额：</h5>
							<input className="hideRecord" type="number"/>
							<input className="money" type="number" placeholder="请输入充值金额100元起充"/>
						</div>
						<div className="rechargeBtn">
							<a href="javascript:;">确认充值</a>
						</div>
					</div>
				</div>
			</div>
    )
  }
}

export default withRouter(Com);