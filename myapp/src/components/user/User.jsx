import React, { Component } from 'react';
import GlobalHeader from '@/components/base/GlobalHeader';
import HeaderNav from '@/components/base/HeaderNav';
import Footer from '@/components/base/Footer';
import Bottom from '@/components/base/Bottom';
import Login from './Login';
class Com extends Component {
  constructor (props) {
    super(props)
    this.state = {
      list: [
      ]
    }
  }

  render () {
    return (
      <div className = "container">
      <GlobalHeader />
      <HeaderNav />
      <Login />
      <Footer />
      <Bottom />
      </div>
    )
  }
}

export default Com;
