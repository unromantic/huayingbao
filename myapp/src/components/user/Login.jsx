import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import './user.scss';
import axios from 'axios'
class Com extends Component {
  constructor(props) {
    super(props)
    this.state = {
      list: []
    }
  }
  login () {
    let username = document.querySelector('.mobile').value;
    let password = document.querySelector('.password').value;
    var modal = document.querySelector('.modal')
    if(/^1[34578]\d{9}$/.test(username)) {
      axios.get('http://jx.xuzhixiang.top/ap/api/login.php', {
        username: username,
        password: password
      }).then(data => {
        console.log(data)
        if (data.status === 200) {
          console.log('请求成功')
          modal.style.display = 'block'
          modal.innerHTML = '登陆成功'
          setInterval(() => {         
            modal.style.display = 'none'
          },2000);
        } else {
          console.log('请求失败')
        }
      })
    } else {
      console.log('手机号格式不正确')
      modal.style.display = 'block'
      modal.innerHTML = '手机号码输入有误'
      setInterval(() => {         
        modal.style.display = 'none'
      },2000);
    }
  }
  render() {
    return(
      <div className="com">
        <div className="log">
          <h4><span></span>老用户登录</h4>
          <div className="logForm">
            <h5>登录我的华赢宝账户</h5>
            <p>没有账户？  <NavLink to="/Reg">注册</NavLink></p>
            <ul>
              <li>
                <span>手机号：</span>
                <input className="hideRecord" type="tel" />
                <input className="mobile" type="tel" placeholder="请输入手机号" />
              </li>
              <li>
                <span>密码：</span>
                <input className="hideRecord" type="password" />
                <input className="password" type="password" placeholder="请输入密码" />
              </li>
            </ul>
            <div className="isAgreement">
              <p><img className="check" id="check" alt="" src={require('./../../img/checkOn.png')} />您的信息已使用128位SGC加密技术，保护数据传输安全</p>
            </div>
            <div className="registerBtn login">
              <button className="sameBtn loginBtn" onClick = { this.login.bind(this) }>立即登录</button>
            </div>
            <NavLink to="/" className="forget">忘记密码？</NavLink>
          </div>
        </div>
        <div className="modal"></div>
      </div>      
    )
  }
}

export default Com;