import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import {withRouter} from 'react-router'
import './user.scss';
import axios from 'axios'
class Com extends Component {
  constructor(props) {
    super(props)
    this.state = {
      list: []
    }
  }
  agree () {
    var check = document.querySelector('.check');
    if(check.getAttribute('src') === 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABWUlEQVQ4T2NkYGBg8JryRI3hz/92Bob/LgwMDHwgMTzgEwMD4x4GFsbKbTkytxghmv+dYGBgECSgEV36PQMLkwWj14THaxkY/geRqBmqnHEdo9eERx+JcDZYAwsTA4OfAQ/DhvNfGP79Bwt9AhkAYRIBshwFGPg4mRk6tr2FqybaAEd1LoYIcz6GguUvGb7/RthJlAHywiwMbUFiDJVrXzE8evcHxa1wA2SFWBgY/jMwPH6PqoCTjZFhYoQ4w7KTnxgO3PyG4VG4ATYqnAxp9oIMdRteMzx4+xuusMpLmOH9t78M0w98wBpKKF6wUuFkyHIQZGjY9JrhzqvfDIGGPAy2alwMZatfMfz5hz2UMcLAVIGDocBViGHV6U8MISZ8DEUrXzK8/vwXZxxhDURDWXaGah8RhratbxjOPfqJN4JxJiQhbiaGd19xuBthJCghUZqUKc1MlGZnAD7ojOeMTau4AAAAAElFTkSuQmCC') {
      check.setAttribute("src", "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABKElEQVQ4T6VTS1LCQBTsFxjygYRAEcuyXHsGD+AJvI0n8DR4Ag/gGdzgwko0wTCEj8gkGWuKAg0YiJVZvunu9+tHAPAaRVepkPeScAMJR8VKHyEhiccmo7tLz3smRRYCT4DsHSUefNKEMVzTix8OJXD7P/IGTcADjYJwerLsX+qOZSJZfm4VEhr5oayave900NAIEZ/tKJUF2oYO17bgjzmk/MlZSYA1Gzjvu3iLOUSaFQreCSgQJCCyPQARLgYu+GyJxerrcBfbGVhGC6rH93hayOK5NrJcIk7mf46q0MJGxEYYc6zTDGribVNH8MHLfbW/BVNnGHQd8PkC3Y6FYMyR5Xl1AYU0WgxnPQfhJMFqLY47u8xImkbI8xMWUXdR28q1j6nuOX8DnqWgPaUF6dMAAAAASUVORK5CYII=")
    } else {
      check.setAttribute("src", "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABWUlEQVQ4T2NkYGBg8JryRI3hz/92Bob/LgwMDHwgMTzgEwMD4x4GFsbKbTkytxghmv+dYGBgECSgEV36PQMLkwWj14THaxkY/geRqBmqnHEdo9eERx+JcDZYAwsTA4OfAQ/DhvNfGP79Bwt9AhkAYRIBshwFGPg4mRk6tr2FqybaAEd1LoYIcz6GguUvGb7/RthJlAHywiwMbUFiDJVrXzE8evcHxa1wA2SFWBgY/jMwPH6PqoCTjZFhYoQ4w7KTnxgO3PyG4VG4ATYqnAxp9oIMdRteMzx4+xuusMpLmOH9t78M0w98wBpKKF6wUuFkyHIQZGjY9JrhzqvfDIGGPAy2alwMZatfMfz5hz2UMcLAVIGDocBViGHV6U8MISZ8DEUrXzK8/vwXZxxhDURDWXaGah8RhratbxjOPfqJN4JxJiQhbiaGd19xuBthJCghUZqUKc1MlGZnAD7ojOeMTau4AAAAAElFTkSuQmCC")
    } 
  }
  register () {
    let username = document.querySelector('.mobile').value;
    let password = document.querySelector('.password').value;
    var check = document.querySelector('.check');
    var modal = document.querySelector('.modal')
    if(check.getAttribute('src') === 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABWUlEQVQ4T2NkYGBg8JryRI3hz/92Bob/LgwMDHwgMTzgEwMD4x4GFsbKbTkytxghmv+dYGBgECSgEV36PQMLkwWj14THaxkY/geRqBmqnHEdo9eERx+JcDZYAwsTA4OfAQ/DhvNfGP79Bwt9AhkAYRIBshwFGPg4mRk6tr2FqybaAEd1LoYIcz6GguUvGb7/RthJlAHywiwMbUFiDJVrXzE8evcHxa1wA2SFWBgY/jMwPH6PqoCTjZFhYoQ4w7KTnxgO3PyG4VG4ATYqnAxp9oIMdRteMzx4+xuusMpLmOH9t78M0w98wBpKKF6wUuFkyHIQZGjY9JrhzqvfDIGGPAy2alwMZatfMfz5hz2UMcLAVIGDocBViGHV6U8MISZ8DEUrXzK8/vwXZxxhDURDWXaGah8RhratbxjOPfqJN4JxJiQhbiaGd19xuBthJCghUZqUKc1MlGZnAD7ojOeMTau4AAAAAElFTkSuQmCC') {
      if(/^1[34578]\d{9}$/.test(username)) {        
        axios.get('http://jx.xuzhixiang.top/ap/api/reg.php', {
          username: username,
          password: password
        }).then(data => {
          console.log(data)
          if (data.status === 200) {
            console.log('请求成功');
            modal.style.display = 'block'
            modal.innerHTML = '注册成功'
            setInterval(() => {         
              modal.style.display = 'none'
            },2000);
            this.props.history.push('/User')
          } else {
            console.log('请求失败')            
          }
        })
      } else {
        console.log('手机号格式不正确')
        modal.style.display = 'block'
        modal.innerHTML = '手机号码输入有误'
        setInterval(() => {         
          modal.style.display = 'none'
        },2000);
      }      
    } else {
      console.log('请先同意用户协议')
      modal.style.display = 'block';
      modal.innerHTML = '请先同意用户协议'
      setInterval(() => {         
        modal.style.display = 'none'
      },3000);
    }
    
  }
  
  
  code () {
    let username = document.querySelector('.mobile').value;
    var modal = document.querySelector('.modal');
    var send = document.querySelector('.send');
    console.log(send.innerHTML)	  
    if(/^1[34578]\d{9}$/.test(username)) {
      axios.get('https://www.daxunxun.com/users/sendCode', {
        tel: username
      }).then(data => {
        console.log(data)
        if (data.status === 200) {
          console.log('请求成功')
          let count = 60;
			    send.innerHTML = count + ' S';
			    let tt = setInterval(() => {         	    
				    count --;
				    send.innerHTML = count + ' S';
				    if (send.innerHTML === -1 + ' S') {
					  	clearInterval(tt)
					  	send.innerHTML = '立即获取'
					  }
				  },1000);
        } else {
          console.log('请求失败')
        }
      })
    } else {
      console.log('手机号格式不正确')
      modal.style.display = 'block'
      modal.innerHTML = '手机号码输入有误'
      setInterval(() => {         
        modal.style.display = 'none'
      },2000);
    }
  }
  render() {
    return(
      <div className="content">
        <div className="reg">
          <h4><span></span>新用户注册</h4>
          <div className="registerForm">
            <h5>免费注册华赢宝账户</h5>
            <p>已有账户？  <NavLink to="/User">登录</NavLink></p>
            <ul>
              <li>
                <span>手机号：</span>
                <input className="hideRecord" type="tel" />
                <input className="mobile" type="tel" placeholder="请输入手机号" />
              </li>
              <li>
                <span>短信验证码：</span>
                <input className="code" type="text" placeholder="请输入验证码" />
                <button className="send disabled clickActive" onClick = { this.code.bind(this) }>立即获取</button>
              </li>
              <li>
                <span>密码：</span>
                <input className="hideRecord" type="password" />
                <input className="password" type="password" placeholder="请输入密码" />
              </li>
              <li>
                <span>邀请码：</span>
                <input className="invite" type="tel" placeholder="请输入验证码(选填)" />
              </li>
            </ul>
            <div className="isAgreement">
              <p><img className="check" id="check" alt="" src={require('./../../img/checkOn.png')} onClick = { this.agree.bind(this) }/>已同意
              <NavLink to="/">《定存计划服务协议》</NavLink>
              <NavLink to="/">《用户授权委托书》</NavLink>
              <NavLink to="/">《风险揭示书》</NavLink>
              </p>
            </div>
            <div className="registerBtn">
              <button className="sameBtn loginBtn" onClick = { this.register.bind(this) }>注册</button>
            </div>
          </div>
        </div>
        <div className="modal"></div>
      </div>      
    )
  }
}

export default withRouter(Com);