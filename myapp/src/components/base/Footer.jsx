import React, { Component } from 'react';
import './base.scss';

class Com extends Component {
  constructor (props) {
    super(props)
    this.state = {
      list: []
    }
  }

  render () {
    return (
      <div className="footer">
        <ul>
          <li>稳健发展</li>
          <li>银行存管</li>
          <li>风险管理</li>
          <li>股东背景</li>
        </ul>
        <ul>
          <li>运营数据</li>
        </ul>
        <ul>
          <li>信息披露</li>
          <li>关于我们</li>
          <li>业务介绍</li>
          <li>团队介绍</li>
        </ul>
        <ul>
          <li>
            <span><img src={require('./../../img/phone.png')} alt=""/></span>
            <dl>
              <dt>
                客服热线：
				      </dt>
              <dd>
                工作时间：9:00～21:00
              </dd>
            </dl>
          </li>
          <li>400-1190-717</li>
        </ul>
        <ul>
          <li>
            <span><img src={require('./../../img/adress.png')} alt=""/></span>
            <dl>
              <dt>
                公司地址：
              </dt>
              <dd>
                杭州市滨江区信诚路33号支氏集团
              </dd>
            </dl>
          </li>
          <li>
            <span><img src={require('./../../img/qq.png')} alt=""/></span>
            <dl>
              <dt>
                QQ粉丝群：
              </dt>
              <dd>
                251094151
              </dd>
            </dl>
          </li>
        </ul>
      </div>
    )
  }
}

export default Com;