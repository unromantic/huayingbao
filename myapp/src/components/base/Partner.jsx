import React, { Component } from 'react';
import './base.scss';
class Com extends Component {
  constructor (props) {
    super(props)
    this.state = {
      list: []
    }
  }

  render () {
    return (
      <div className="partner">
        <div className='partner_tit'>
          <img src={require('./../../img/box_icon.png')} alt=""/>
          <span>合作单位</span>
        </div>
        <div className="partner_cont">
          <div className="swiper-container">
            <div className="swiper-wrapper">
              <div className="swiper-slide"><img src={require('./../../img/hz_1.png')} alt="" /></div>
              <div className="swiper-slide"><img src={require('./../../img/hz_2.png')} alt="" /></div>
              <div className="swiper-slide"><img src={require('./../../img/hz_3.png')} alt="" /></div>
              <div className="swiper-slide"><img src={require('./../../img/hz_4.png')} alt="" /></div>
              <div className="swiper-slide"><img src={require('./../../img/hz_5.png')} alt="" /></div>
              <div className="swiper-slide"><img src={require('./../../img/hz_6.png')} alt="" /></div>
              <div className="swiper-slide"><img src={require('./../../img/hz_7.png')} alt="" /></div>
            </div>
            {/* <!-- Add Pagination --> */}
            <div className="swiper-pagination"></div>
            {/* <!-- 如果需要导航按钮 --> */}
            <div className="swiper-button-prev"></div>
            <div className="swiper-button-next"></div>
          </div>
        </div>
        
      </div>
    )
  }
}

export default Com;