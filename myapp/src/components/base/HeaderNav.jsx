import React, { Component } from 'react';
import { NavLink, Link } from 'react-router-dom';
import { Menu, Dropdown } from 'antd';
import './base.scss';

const menu = (
  <Menu>
    <Menu.Item>
      <Link to='/Info/Default'>关于华赢宝</Link>
    </Menu.Item>
    <Menu.Item>
      <Link to="/Info/Team">团队介绍</Link>
    </Menu.Item>
    <Menu.Item>
      <Link to="/Info/Business">业务介绍</Link>
    </Menu.Item>
    <Menu.Item>
      <Link to="/Info/Operate">运营数据</Link>
    </Menu.Item>
    <Menu.Item>
      <Link to="/Info/Media">媒体报道</Link>
    </Menu.Item>
    <Menu.Item>
      <Link to="/Info/Company">公司动态</Link>
    </Menu.Item>
    <Menu.Item>
      <Link to="/info/Mechanism">从业机构信息</Link>
    </Menu.Item>
    <Menu.Item>
      <Link to="/info/Law">相关法律法规披露</Link>
    </Menu.Item>
  </Menu>
)

class Com extends Component {
  constructor (props) {
    super(props)
    this.state = {
      list: []
    }
  }

  render () {
    return (
      <div className = "headerBox">
        <h1 className="logo">
          <img src={require('./../../img/logo.png')} alt=""/>
        </h1>
        <div className="nav">
          <ul>
            <li>
              <NavLink to="/index" replace>首页</NavLink>
            </li>
            <li>
              <NavLink to="/lend" replace>我要出借</NavLink>
            </li>
            <li>
              <NavLink to="/dev" replace>稳健发展</NavLink>
            </li>
            <li>
              <Dropdown overlay={menu}>
                <NavLink to="/info" replace>信息披露</NavLink>
              </Dropdown>
            </li>
            <li>
              <NavLink to="/user" replace>我的账户</NavLink>
            </li>
          </ul>
        </div>
      </div>
    )
  }
}

export default Com;
