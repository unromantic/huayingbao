import React, { Component } from 'react';
import { BackTop } from 'antd';
import './base.scss';

class Com extends Component {

  render () {
    return (
      <BackTop>
        <div className="gotop">
          <div className="common-toolbar common-toolbar-download">
            <div className="common-toolbar-layer">
              <div className="app-qr-dl"><img src={require('./../../img/app.png')} alt=""/></div>
              <em>扫码下载app领红包</em>
            </div>
          </div>
          <div className="common-toolbar common-toolbar-service">
            <div className="common-toolbar-layer">
              <div className="app-qr-dl"><img src={require('./../../img/weichat.png')} alt=""/></div>
              <em>关注微信福利抢先知</em>
            </div>
          </div>
          <div className="common-toolbar common-toolbar-kefu">
            <div className="common-toolbar-layer">
              <div className="toolbar-text">
                <ul>
                  <li>客服热线：<br/>400-1190-717</li>
                  <li>工作时间：<br/>9:00 ~ 21:00</li>
                  <li>QQ粉丝群：<br/>251094151</li>
                </ul>
              </div>
            </div>
          </div>
          <div className="common-toolbar common-toolbar-gotop">
            <div className="ant-back-top-inner"></div>
          </div>
        </div>
      </BackTop>
    )
  }
}

export default Com;