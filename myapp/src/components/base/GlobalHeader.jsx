import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './base.scss';

class Com extends Component {
  constructor (props) {
    super(props)
    this.state = {
      list: []
    }
  }

  render () {
    return (
      <div className = "container_s">
        <div className="header">
          <div className="left">
            <ul>
              <li className='service'>
                <span className='iconfont icon-weibiaoti'></span> 
                <span>客服热线： 400-1190-717（工作时间 09:00-21:00）</span>
                </li>
              <li className='weixin'>
                <span className='iconfont icon-weixin'></span>
                <img src={require('./../../img/微信.jpg')} alt=""/>
              </li>
              <li className='qq'>
                <span className='iconfont icon-qq'></span>
                <img src={require('./../../img/qqqun.png')} alt=""/>
              </li>
            </ul>
          </div>
          <div className="right">
            <ul>
              <li className="login"><Link to="./User">登录</Link></li>
              <li className="fengexian">/</li>
              <li className="register"><Link to="./Reg">注册</Link></li>
              <li className='app'>
                <span className='iconfont icon-app'></span>
                <span>手机客户端</span>
                <img src={require('./../../img/app.jpg')} alt=""/></li>
              <li>市场有风险，出借需谨慎</li>
            </ul>
          </div>
        </div>
      </div>
    )
  }
}

export default Com;
