import React, { Component } from 'react';
import './base.scss';

class Com extends Component {
  render () {
    return (
      <div className = "bottom">
        <span>Copyright ©2015 - 2018 杭州华赢宝网络科技有限公司</span>
        <span>
          <img src={require('./../../img/wangbei.png')} alt=""/>
          <a href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=33010802005885">浙公网安备：33010402000327</a>  浙ICP备16046593号-1  ICP经营许可证编号：浙B2-20171023
        </span>
      </div>
    )
  }
}

export default Com;