import React, { Component } from 'react';
import './base.scss';
import { Carousel } from 'antd';
// import axios from 'axios';
// import Carousel from 'antd/lib/carousel';

class Com extends Component {
  constructor (props) {
    super(props)
    this.state = {
      bannerList: []
    }
  }

  componentDidMount () {
        
        // axios.get('http://47.100.248.210/api/banner')
        // .then( (response)=> {
        //   this.setState({
        //   bannerList: response
        //   })
        // })
        // .catch(function (error) {
        //     console.log(error);
        // })
        fetch('http://aochuang.daxunxun.com/api/banner')
          .then(res => res.json())
          .then(data => {
            // console.log(data)
            this.setState({
              bannerList: data
            })
          })
  }

  render () {
    console.log(this.state.bannerList)
    const bannerList = this.state.bannerList
    return (
      
      <div className = "banner">
         <Carousel autoplay>
            {
              bannerList.map((item, index) => {
                return (
                  <div key={index}>
                    <img src={item.img} alt=""/>
                  </div>
                )
              })
            }
          {/* <div><h3><img src={require('./../../img/banner1.png')} alt=""/></h3></div>
          <div><h3><img src={require('./../../img/banner2.jpg')} alt=""/></h3></div>
          <div><img src={require('./../../img/banner3.jpg')} alt=""/></div>
          <div><img src={require('./../../img/banner4.png')} alt=""/></div>
          <div><img src={require('./../../img/banner5.png')} alt=""/></div>
          <div><img src={require('./../../img/banner6.jpg')} alt=""/></div>
          <div><img src={require('./../../img/banner7.png')} alt=""/></div>
          <div><img src={require('./../../img/banner8.png')} alt=""/></div>
          <div><img src={require('./../../img/banner9.jpg')} alt=""/></div> */}
        </Carousel>
        <div className="login_box">
          <ul>
            <li>借贷双方约定利率</li>
            <li>8.5%-12.0%</li>
            <li><img src={require('./../../img/downapp.png')} alt=""/></li>
            <li>扫码下载APP 舒心出借稳收益</li>
          </ul>
        </div>
      </div>
    )
  }
}

export default Com;