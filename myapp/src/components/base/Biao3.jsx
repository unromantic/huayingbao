import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './base.scss';

class Com extends Component {
  constructor (props) {
    super(props)
    this.state = {
      list: []
    }
  }

  render () {
    return (
      <div className="biao">
        <img src={require('./../../img/biao_img3.png')} alt=""></img>
        <div className="biao_left">
          <div className="biao_head">
            <span className="first">180天期产品第65期</span>
          </div>
          <div className="biao_cont">
            <ul>
              <li>
                <dl>
                  <dt><span id="interest5">11</span>%</dt>
                  <dd>借贷双方约定利率</dd>
                </dl>
              </li>
              <li>
                <dl>
                  <dt>
                    <span id="period5">180</span>天
                  </dt>
                  <dd>出借期限</dd>
                </dl>
              </li>
            </ul>
          </div>
          <div className="biao_head">
            <span id="name6">360天期产品第36期</span>
          </div>
          <div className="biao_cont">
            <ul>
              <li>
                <dl>
                  <dt><span id="interest5">12</span>%</dt>
                  <dd>借贷双方约定利率</dd>
                </dl>
              </li>
              <li>
                <dl>
                  <dt>
                    <span id="period5">360</span>天
                  </dt>
                  <dd>出借期限</dd>
                </dl>
              </li>
            </ul>
          </div>
          <div className="biao_btn biao_btn1"><Link to="./lend/child/Product">立即出借</Link></div>
          <div className="biao_btn biao_btn2"><Link to="./lend/child/Product">立即出借</Link></div>
          <div className="biao_btn_bottom biao_btn_bottom1">
            <div className='graph'></div>
          </div>
          <div className="biao_btn_bottom biao_btn_bottom2">
            <div className='graph'></div>
          </div>
        </div>
        <div className="biao_right">
        <div className="lanmu_box">
            <div className="lanmu_box_title">
              <img src={require('./../../img/icon_12.png')} alt=""></img>媒体报道
            </div>
            <div className="lanmu_box_cont">
              <ul>
                <li>
                  <a target="_blank" href="./">
                    <div className="bold"></div>
                    <span className="float_l">华赢宝携手三家优质资产对接，加速合规建设</span>
                  </a>
                </li>
                <li><a target="_blank" href="./"><div className="bold"></div><span className="float_l">华赢宝赞助2017萧山广场舞大赛完美结束</span></a></li>
                <li><a target="_blank" href="./"><div className="bold"></div><span className="float_l">支氏控股集团华赢宝发布验资报告</span></a></li>
                <li><a target="_blank" href="./"><div className="bold"></div><span className="float_l">「华赢宝征文活动」有华赢宝的日子，树桩吐绿，鸟语花香</span></a></li>
                <li><a target="_blank" href="./"><div className="bold"></div><span className="float_l">华赢宝累计成交突破10亿元</span></a></li>
                <li><a target="_blank" href="./"><div className="bold"></div><span className="float_l">华赢宝获批ICP证，积极响应监管政策变化</span></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Com;