import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './base.scss';

class Com extends Component {
  constructor (props) {
    super(props)
    this.state = {
      list: []
    }
  }

  render () {
    return (
      <div className="biao">
        <img src={require('./../../img/biao_img2x.png')} alt=""></img>
        <div className="biao_left">
          <div className="biao_head">
            <span className="first">初次见面礼</span>
            <span className="new">新手专享</span>
            <span className="only">仅此一次</span>
          </div>
          <div className="biao_cont">
            <ul>
              <li>
                <dl>
                  <dt><span id="interest5">15</span>%</dt>
                  <dd>借贷双方约定利率</dd>
                </dl>
              </li>
              <li>
                <dl>
                  <dt>
                    <span id="period5">10</span>天
                  </dt>
                  <dd>出借期限</dd>
                </dl>
              </li>
            </ul>
          </div>
          <div className="biao_head">
            <span id="name6">理财日产品第35期</span>
            <span className="only">限时限量</span>
            <span className="only">相约15号</span>
          </div>
          <div className="biao_cont">
            <ul>
              <li>
                <dl>
                  <dt><span id="interest5">24</span>%</dt>
                  <dd>借贷双方约定利率</dd>
                </dl>
              </li>
              <li>
                <dl>
                  <dt>
                    <span id="period5">10</span>天
                  </dt>
                  <dd>出借期限</dd>
                </dl>
              </li>
            </ul>
          </div>
          <div className="biao_btn biao_btn1"><Link to="./lend/child/Product">立即出借</Link></div>
          <div className="biao_btn biao_btn2"><Link to="./lend/child/Product">立即出借</Link></div>
          <div className="biao_btn_bottom biao_btn_bottom1">
            <div className='graph'></div>
          </div>
          <div className="biao_btn_bottom biao_btn_bottom2">
            <div className='graph'></div>
          </div>
        </div>
        <div className="biao_right">
          <a href="javascript:;"><img src={require('./../../img/xsyd2.png')} alt=''></img></a>
          <a href="javascript:;"><img src={require('./../../img/xsyd3.png')} alt=''></img></a>
        </div>
      </div>
    )
  }
}

export default Com;