import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Carousel } from 'antd';
import './base.scss';

class Com extends Component {
  constructor (props) {
    super(props)
    this.state = {
      list: [],
      dots: false
    }
  }
  componentDidMount () {
    fetch('https://aochuang.daxunxun.com/api/lenders')
      .then(res => res.json())
      .then(data => {
        // console.log(data)
        this.setState({
          list: data
        })
      })
  }

  render () {
    return (
      <div className="biao">
        <img src={require('./../../img/biao_img22x.png')} alt=""></img>
        <div className="biao_left">
          <div className="biao_head">
            <span className="first">30天期产品第249期</span>
          </div>
          <div className="biao_cont">
            <ul>
              <li>
                <dl>
                  <dt><span id="interest5">8.5</span>%</dt>
                  <dd>借贷双方约定利率</dd>
                </dl>
              </li>
              <li>
                <dl>
                  <dt>
                    <span id="period5">30</span>天
                  </dt>
                  <dd>出借期限</dd>
                </dl>
              </li>
            </ul>
          </div>
          <div className="biao_head">
            <span id="name6">90天期产品第44期</span>
          </div>
          <div className="biao_cont">
            <ul>
              <li>
                <dl>
                  <dt><span id="interest5">9.5</span>%</dt>
                  <dd>借贷双方约定利率</dd>
                </dl>
              </li>
              <li>
                <dl>
                  <dt>
                    <span id="period5">90</span>天
                  </dt>
                  <dd>出借期限</dd>
                </dl>
              </li>
            </ul>
          </div>
          <div className="biao_btn biao_btn1"><Link to="./lend/child/Product">立即出借</Link></div>
          <div className="biao_btn biao_btn2"><Link to="./lend/child/Product">立即出借</Link></div>
          <div className="biao_btn_bottom biao_btn_bottom1">
            <div className='graph'></div>
          </div>
          <div className="biao_btn_bottom biao_btn_bottom2">
            <div className='graph'></div>
          </div>
        </div>
        <div className="biao_right">
          <div className="lanmu_box">
            <div className="lanmu_box_title">
              <img src={require('./../../img/icon_11.png')} alt=""></img>当月出借排行
            </div>
            <div className="lanmu_box_cont">
              <Carousel vertical autoplay>
                <div>
                  <span className="redio sp"><img src={require('./../../img/no_1.png')} alt=""></img></span>
                  <span className="users">139****1544</span>
                  <span className="money">320000.00</span>
                </div>
                <div>
                  <span className="redio sp"><img src={require('./../../img/no_2.png')} alt=""></img></span>
                  <span className="users">139****1564</span>
                  <span className="money">110000.00</span>
                </div>
                <div>
                  <span className="redio sp"><img src={require('./../../img/no_3.png')} alt=""></img></span>
                  <span className="users">132****1613</span>
                  <span className="money">92173.47</span>
                </div>
                {
                  this.state.list.map((item, index) => {
                    return (
                      <div key={index}>
                        <span className="redio">{index+4}</span>
                        <span className="users">{item.phone}</span>
                        <span className="money">{item.money}</span>
                      </div>
                    )
                  })
                }
              </Carousel>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Com;